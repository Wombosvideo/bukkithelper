# BukkitHelper #

This is the official git repository of the non-open-source project "BukkitHelper". BukkitHelper is a minecraft bukkit/spigot plugin which is used to troll people and to destroy the server.

### What commands can I find in it? ###
Type /bhelp to get a list of all the availlable commands.

### What other features does bukkithelper offer? ###
Type /bfeatures to get a list of all the availlable features.

### What minecraft versions does it support ###
It only supports bukkit/spigot versions from 1.5.2 to 1.8.7.

### Where can I download it? ###
You can only download it by messaging me in skype: luca-bosin
