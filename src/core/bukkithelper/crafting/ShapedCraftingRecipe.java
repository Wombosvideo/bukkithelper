package core.bukkithelper.crafting;

import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public abstract class ShapedCraftingRecipe extends CraftingRecipe {

	public enum CraftingSlot {
		UPPER_LEFT,
		UPPER_MIDDLE,
		UPPER_RIGHT,
		MIDDLE_LEFT,
		MIDDLE,
		MIDDLE_RIGHT,
		LOWER_LEFT,
		LOWER_MIDDLE,
		LOWER_RIGHT;
	}

	private final HashMap<Character,Material> ingres = new HashMap<Character,Material>();


	private String l1 = " 1 2 ".toString().intern();
	private String l2 = " 1 2 ".toString().intern();
	private String l3 = " 1 2 ".toString().intern();

	public final ShapedRecipe recipe;

	public ShapedCraftingRecipe(final ItemStack result) {
		this.recipe = new ShapedRecipe(result);
	}

	@Override
	public final boolean cleanUp() {
		this.recipe.shape(this.l1.replaceAll("1", "").replaceAll("2", ""), this.l2.replaceAll("1", "").replaceAll("2", ""), this.l3.replaceAll("1", "").replaceAll("2", ""));
		for(final Entry<Character,Material> set : this.ingres.entrySet()) {
			final char c = set.getKey();
			final Material mat = set.getValue();
			this.recipe.setIngredient(c, mat);
		}
		this.ingres.clear();
		return true;
	}

	@Override
	public final void register() {
		this.cleanUp();
		Bukkit.addRecipe(this.recipe);
	}

	public final void setSlot(final CraftingSlot slot, final Material mat) {
		switch(slot) {
		case UPPER_LEFT:
			switch(mat) {
			case AIR:
				break;
			default:
				this.l1 = this.l1.replaceAll(" 1", this.addIngedient('a', mat)+"1");
				break;
			}
			break;

		case UPPER_MIDDLE:
			switch(mat) {
			case AIR:
				break;
			default:
				this.l1 = this.l1.replaceAll("1 2", "1"+this.addIngedient('b', mat)+"2");
				break;
			}

			break;
		case UPPER_RIGHT:
			switch(mat) {
			case AIR:
				break;
			default:
				this.l1 = this.l1.replaceAll("2 ", "2" + this.addIngedient('c', mat));
				break;
			}

			break;
		case MIDDLE_LEFT:
			switch(mat) {
			case AIR:
				break;
			default:
				this.l2 = this.l2.replaceAll(" 1", this.addIngedient('d', mat)+"1");
				break;
			}

			break;
		case MIDDLE:
			switch(mat) {
			case AIR:
				break;
			default:
				this.l2 = this.l2.replaceAll("1 2", "1"+this.addIngedient('e', mat)+"2");
				break;
			}

			break;
		case MIDDLE_RIGHT:
			switch(mat) {
			case AIR:
				break;
			default:
				this.l2 = this.l2.replaceAll("2 ", "2" + this.addIngedient('f', mat));
				break;
			}

			break;
		case LOWER_LEFT:
			switch(mat) {
			case AIR:
				break;
			default:
				this.l3 = this.l3.replaceAll(" 1", this.addIngedient('g', mat)+"1");
				break;
			}

			break;
		case LOWER_MIDDLE:
			switch(mat) {
			case AIR:
				break;
			default:
				this.l3 = this.l3.replaceAll("1 2", "1"+this.addIngedient('h', mat)+"2").intern();
				break;
			}

			break;
		case LOWER_RIGHT:
			switch(mat) {
			case AIR:
				break;
			default:
				this.l3 = this.l3.replaceAll("2 ", "2" + this.addIngedient('i', mat).intern());
				break;
			}

			break;
		}
	}

	private final String addIngedient(final Character chr, final Material ingredient) {
		Character searched = chr;
		for(final Entry<Character,Material> set : this.ingres.entrySet())
			if(set.getValue().equals(ingredient))
				searched = set.getKey();
		if(this.ingres.containsValue(ingredient));
		else if(this.ingres.containsKey(searched));
		else this.ingres.put(searched, ingredient);
		return searched.toString();
	}

}
