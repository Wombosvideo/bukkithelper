package core.bukkithelper.crafting;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SwordRecipe extends ShapedCraftingRecipe {

	public static ItemStack getItem() {
		final ItemStack is = new ItemStack(Material.DIAMOND_SWORD);
		final ItemMeta im = is.getItemMeta();
		im.setDisplayName("§6Overpowered Diamond Sword");
		is.setItemMeta(im);
		is.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 10000);
		is.addUnsafeEnchantment(Enchantment.ARROW_FIRE, 10000);
		is.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 10000);
		is.addUnsafeEnchantment(Enchantment.ARROW_KNOCKBACK, 10000);
		is.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 10000);
		is.addUnsafeEnchantment(Enchantment.DAMAGE_ARTHROPODS, 10000);
		is.addUnsafeEnchantment(Enchantment.DAMAGE_UNDEAD, 10000);
		is.addUnsafeEnchantment(Enchantment.DIG_SPEED, 10000);
		is.addUnsafeEnchantment(Enchantment.DURABILITY, 10000);
		is.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 10000);
		is.addUnsafeEnchantment(Enchantment.KNOCKBACK, 10000);
		is.addUnsafeEnchantment(Enchantment.LOOT_BONUS_BLOCKS, 1000);
		is.addUnsafeEnchantment(Enchantment.LOOT_BONUS_MOBS, 1000);
		is.addUnsafeEnchantment(Enchantment.OXYGEN, 10000);
		is.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10000);
		is.addUnsafeEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 10000);
		is.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 10000);
		is.addUnsafeEnchantment(Enchantment.PROTECTION_FIRE, 10000);
		is.addUnsafeEnchantment(Enchantment.PROTECTION_PROJECTILE, 10000);
		is.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 10000);
		is.addUnsafeEnchantment(Enchantment.THORNS, 10000);
		is.addUnsafeEnchantment(Enchantment.WATER_WORKER, 10000);
		is.setDurability(Short.MAX_VALUE);
		return is;
	}

	public SwordRecipe() {
		super(getItem());
		this.setSlot(CraftingSlot.UPPER_LEFT, Material.COBBLESTONE);
		this.setSlot(CraftingSlot.MIDDLE, Material.COBBLESTONE);
		this.setSlot(CraftingSlot.LOWER_RIGHT, Material.STICK);
	}

}
