package core.bukkithelper.crafting;


public abstract class CraftingRecipe {

	public abstract boolean cleanUp();

	public abstract void register();

}
