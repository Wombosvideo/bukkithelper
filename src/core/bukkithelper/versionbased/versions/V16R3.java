package core.bukkithelper.versionbased.versions;

import core.bukkithelper.versionbased.ServerVersion;
import core.bukkithelper.versionbased.reflected.DefaultBanManager;
import core.bukkithelper.versionbased.reflected.LegacyEquipmentPacketSender;

public class V16R3 extends ServerVersion {

	public V16R3() {
		super(false, "v1_6_R3", new LegacyEquipmentPacketSender(), new DefaultBanManager());
	}

}
