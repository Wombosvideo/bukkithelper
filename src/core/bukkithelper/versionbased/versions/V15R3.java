package core.bukkithelper.versionbased.versions;

import core.bukkithelper.versionbased.ServerVersion;
import core.bukkithelper.versionbased.reflected.DefaultBanManager;
import core.bukkithelper.versionbased.reflected.LegacyEquipmentPacketSender;

public class V15R3 extends ServerVersion {

	public V15R3() {
		super(false, "v1_5_R3", new LegacyEquipmentPacketSender(), new DefaultBanManager());
	}

}
