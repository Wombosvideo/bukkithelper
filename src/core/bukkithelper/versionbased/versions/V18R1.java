package core.bukkithelper.versionbased.versions;

import core.bukkithelper.versionbased.ServerVersion;
import core.bukkithelper.versionbased.reflected.DefaultBanManager;
import core.bukkithelper.versionbased.reflected.DefaultEquipmentPacketSender;

public class V18R1 extends ServerVersion {

	public V18R1() {
		super(true, "v1_8_R1", new DefaultEquipmentPacketSender(), new DefaultBanManager());
	}

}
