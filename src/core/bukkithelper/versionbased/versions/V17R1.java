package core.bukkithelper.versionbased.versions;

import core.bukkithelper.versionbased.ServerVersion;
import core.bukkithelper.versionbased.reflected.DefaultBanManager;
import core.bukkithelper.versionbased.reflected.DefaultEquipmentPacketSender;

public class V17R1 extends ServerVersion {

	public V17R1() {
		super(false, "v1_7_R1", new DefaultEquipmentPacketSender(), new DefaultBanManager());
	}

}
