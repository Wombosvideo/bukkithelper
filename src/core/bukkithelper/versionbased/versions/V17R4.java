package core.bukkithelper.versionbased.versions;

import core.bukkithelper.versionbased.ServerVersion;
import core.bukkithelper.versionbased.reflected.DefaultBanManager;
import core.bukkithelper.versionbased.reflected.DefaultEquipmentPacketSender;

public class V17R4 extends ServerVersion {

	public V17R4() {
		super(false, "v1_7_R4", new DefaultEquipmentPacketSender(), new DefaultBanManager());
	}

}
