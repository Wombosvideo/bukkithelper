package core.bukkithelper.versionbased.versions;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import core.bukkithelper.utils.Reflections;
import core.bukkithelper.versionbased.PacketSender;
import core.bukkithelper.versionbased.VersionBase;


public class ReflectedPacketSender extends PacketSender {

	public static void sSendNotTo(final Object packet, final ArrayList<Player> ps) {
		for(final Player a : VersionBase.getOnlinePlayers())
			if(!ps.contains(a))
				sSendTo(packet, a);
	}



	public static void sSendNotTo(final Object packet, final Player p) {
		final ArrayList<Player> pl = new ArrayList<Player>();
		pl.add(p);
		sSendNotTo(packet, pl);
	}

	public static void sSendTo(final Object packet, final Player p){
		try{
			final Object handle = Reflections.getHandle(p);
			final Object playerConnection = Reflections.getField(handle.getClass(), "playerConnection").get(handle);
			Reflections.getMethod(playerConnection.getClass(), "sendPacket", Reflections.getNMSClass("Packet")).invoke(playerConnection, packet);
		}catch(final Exception ex){}
	}

	public static void sSendToAll(final Object packet) {
		for(final Player p : VersionBase.getOnlinePlayers())
			sSendTo(packet, p);
	}

	@Override
	public void sendTo(final Object packet, final Player p) {
		ReflectedPacketSender.sSendTo(packet, p);
	}

}
