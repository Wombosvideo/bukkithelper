package core.bukkithelper.versionbased;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.BadShit;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import core.bukkithelper.utils.Reflections;

public class VersionBase {

	public static final ServerVersion version = getVersion();

	public static final String base64decode(final String base64){
		try {
			return new String((byte[]) Reflections.getMethod(getDeprecatedUtil("org.apache.commons.codec.binary.Base64"), "decodeBase64String", String.class).invoke(null, base64));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		}
		return null;
	}

	public static final String base64encode(final String text){
		try {
			return (String) Reflections.getMethod(getDeprecatedUtil("org.apache.commons.codec.binary.Base64"), "encodeBase64String", byte[].class).invoke(null, text.getBytes());
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		}
		return null;
	}

	public static final int getFood(final Player p){
		final Object a = get(p, "getFoodLevel");
		return a.equals(null) ? -1 : a.getClass().equals(int.class) ? (int) a : (int) (double) a;
	}

	public static final int getHealth(final Player p){
		final Object a = get(p, "getHealth");
		return a.equals(null) ? -1 : a.getClass().equals(int.class) ? (int) a : (int) (double) a;
	}

	public static final OfflinePlayer getOfflinePlayer(final String name){
		Object offlinePlayer = null;
		try {
			offlinePlayer = Reflections.getMethod(Class.forName("org.bukkit.Bukkit"), "getOfflinePlayer", String.class).invoke(null, name);
		} catch (final Exception e) {
			return null;
		}
		return (OfflinePlayer) offlinePlayer;
	}

	@SuppressWarnings("unchecked")
	public static final List<Player> getOnlinePlayers(){
		Object onlinePlayers = null;
		try {
			onlinePlayers = Reflections.getMethod(Class.forName("org.bukkit.Bukkit"), "getOnlinePlayers").invoke(null, new Object[0]);
		} catch (final Exception e) {
			return null;
		}
		if(onlinePlayers instanceof Player[])
			return new ArrayList<Player>((Collection<? extends Player>) Arrays.asList(onlinePlayers));
		else if(onlinePlayers instanceof Collection<?>)
			return new ArrayList<Player>((Collection<? extends Player>) onlinePlayers);
		else
			return Lists.newArrayList();
	}

	public static final Player getPlayer(final String name){
		Object player = null;
		try {
			player = Reflections.getMethod(Class.forName("org.bukkit.Bukkit"), "getPlayer", String.class).invoke(null, name);
		} catch (final Exception e) {
			return null;
		}
		return (Player) player;
	}

	public static final void setFood(final Player p, final int food){
		set(p, "setFoodLevel", food, food);
	}

	public static final void setHealth(final Player p, final int health){
		p.setHealth(health);
	}

	private static final Object get(final Player p, final String method){
		final Method m = Reflections.getMethod(p.getClass(), method);
		try {
			return m.invoke(p);
		} catch (final IllegalAccessException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		} catch (final IllegalArgumentException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		} catch (final InvocationTargetException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	private static final Class getDeprecatedUtil(final String name){
		try{
			return Class.forName("net.minecraft.util." + name);
		}catch(final ClassNotFoundException e){
			try {
				return Class.forName(name);
			} catch (final ClassNotFoundException e1) {
				return null;
			}
		}
	}

	private static final ServerVersion getVersion(){
		return getVersionById(Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3]);
	}

	@SuppressWarnings("unchecked")
	private static final ServerVersion getVersionById(final String id){
		Class<? extends ServerVersion> clas = null;
		try{
			clas = (Class<? extends ServerVersion>) Class.forName("core.bukkithelper.versionbased.versions." + id.toUpperCase().replaceAll("_", ""));
		}catch(final ClassNotFoundException ex){
			return null;
		}
		try {
			return clas.newInstance();
		} catch (final InstantiationException e) {
			return null;
		} catch (final IllegalAccessException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private static final <T> void set(final Player p, final String method, final Object object, final T c){
		boolean a = false;
		final boolean b = object.getClass().equals(int.class);
		Method m = Reflections.getMethod(p.getClass(), method, b?double.class:object.getClass());
		if(b && m.equals(null)){
			m = Reflections.getMethod(p.getClass(), method, int.class);
			a = true;
		}
		try {
			if(b){
				if(a)
					m.invoke(p, object);
				else
					m.invoke(p, ((Double)object).intValue());
			} else
				m.
				invoke
				(p,
						(T)object);

		} catch (final IllegalAccessException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		} catch (final IllegalArgumentException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		} catch (final InvocationTargetException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		}
	}

}
