package core.bukkithelper.versionbased;

import core.bukkithelper.versionbased.base.BanManager;
import core.bukkithelper.versionbased.base.EquipmentPacketSender;

public class ServerVersion {

	private final boolean gm3supported;
	private final String id;
	private final EquipmentPacketSender eps;
	private final BanManager bm;

	public ServerVersion(final boolean gm3supported, final String id, final EquipmentPacketSender eps, final BanManager bm){
		this.gm3supported = gm3supported;
		this.id = id;
		this.eps = eps;
		this.bm = bm;
	}

	public final BanManager getBanManager(){
		return this.bm;
	}

	public final EquipmentPacketSender getEquipmentPacketSender() {
		return this.eps;
	}

	public final String getId(){
		return this.id;
	}

	public final boolean isSpectatorModeSupported(){
		return this.gm3supported;
	}

}
