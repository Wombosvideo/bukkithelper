package core.bukkithelper.versionbased.reflected;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;

import org.bukkit.Bukkit;

import core.bukkithelper.utils.Reflections;
import core.bukkithelper.versionbased.base.BanManager;

public class DefaultBanManager implements BanManager {

	@Override
	public final boolean ban(final String name){
		return this.ban(name, null, null, "Server", "You are banned by an operator.");
	}

	@Override
	public final boolean ban(final String name, final Date banned, final Date expires, final String bannedBy, final String reason){
		try{
			final Object server = Bukkit.getServer();
			final Object gameprofile = Reflections.getMethod(Reflections.getNMSClass("UserCache"), "getProfile", String.class).invoke(server, name);
			if(gameprofile == null)
				return false;
			final Object banEntry = Reflections.getConstructor(Reflections.getNMSClass("GameProfileBanEntry"), gameprofile.getClass(), Date.class, String.class, Date.class, String.class).newInstance(gameprofile, banned, bannedBy, expires, reason);
			final Object playerList = Reflections.getMethod(server.getClass(), "getPlayerList").invoke(server);
			final Object profileBans = Reflections.getMethod(playerList.getClass(), "getProfileBans").invoke(playerList);
			Reflections.getMethod(profileBans.getClass(), "add", Reflections.getNMSClass("GameProfileBanEntry")).invoke(profileBans, banEntry);
			final Object ep = Reflections.getMethod(playerList.getClass(), "getPlayer", String.class).invoke(playerList, name);
			if(ep != null){
				final Object playerConnection = Reflections.getField(ep.getClass(), "playerConnection").get(ep);
				Reflections.getMethod(playerConnection.getClass(), "disconnect", String.class).invoke(playerConnection, "You are banned from this server.");
			}
			return true;
		}catch(final Exception e){
			return false;
		}
	}

	@Override
	public final boolean banIp(final String ban){
		try{
			final Matcher match = a.matcher(ban);
			if(match.matches())
				return this.banIp("Server", ban, "You are banned by an operator.", null, null);
			else{
				final Object server = Bukkit.getServer();
				final Object playerList = Reflections.getMethod(server.getClass(), "getPlayerList").invoke(server);
				final Object ep = Reflections.getMethod(playerList.getClass(), "getPlayer", String.class).invoke(playerList, ban);
				if(ep != null)
					return this.banIp("Server", Reflections.getMethod(ep.getClass(), "s").invoke(ep).toString(), "You are banned by an operator", null, null);
				else
					return false;
			}
		}catch(final Exception ex){
			return false;
		}
	}

	@Override
	public final boolean banIp(final String name, final String ip, final String reason, final Date banned, final Date expires){
		try{
			final Object be = Reflections.getConstructor(Reflections.getNMSClass("IpBanEntry"), String.class, Date.class, String.class, Date.class, String.class).newInstance(ip, null, name, null, reason);
			final Object server = Bukkit.getServer();
			final Object playerList = Reflections.getMethod(server.getClass(), "getPlayerList").invoke(server);
			final Object ipBans = Reflections.getMethod(playerList.getClass(), "getIPBans").invoke(playerList);
			Reflections.getMethod(ipBans.getClass(), "add", be.getClass()).invoke(ipBans, be);
			@SuppressWarnings("rawtypes")
			final List plist = (List) Reflections.getMethod(playerList.getClass(), "b", String.class).invoke(playerList, ip);
			final String[] aos = new String[plist.size()];
			int i = 0;
			for(final Object ep : plist){
				final Object playerConnection = Reflections.getField(ep.getClass(), "playerConnection").get(ep);
				Reflections.getMethod(playerConnection.getClass(), "disconnect", String.class).invoke(playerConnection, "You have been IP banned.");
				aos[i++] = Reflections.getMethod(ep.getClass(), "getName").invoke(ep).toString();
			}
			return true;
		}catch(final Exception ex){
			return false;
		}
	}

	@Override
	public final List<String> getBans(){
		List<String> players = new ArrayList<String>();
		try{
			final Object server = Bukkit.getServer();
			final Object playerList = Reflections.getMethod(server.getClass(), "getPlayerList").invoke(server);
			final Object profileBans = Reflections.getMethod(playerList.getClass(), "getProfileBans").invoke(playerList);
			final String[] entries = (String[]) Reflections.getMethod(profileBans.getClass(), "getEntries").invoke(profileBans);
			players = Arrays.asList(entries);
			return players;
		}catch(final Exception e){
			return players;
		}
	}

	@Override
	public final List<String> getIPBans(){
		List<String> ips = new ArrayList<String>();
		try{
			final Object server = Bukkit.getServer();
			final Object playerList = Reflections.getMethod(server.getClass(), "getPlayerList").invoke(server);
			final Object ipBans = Reflections.getMethod(playerList.getClass(), "getIPBans").invoke(playerList);
			final String[] entries = (String[]) Reflections.getMethod(ipBans.getClass(), "getEntries").invoke(ipBans);
			ips = Arrays.asList(entries);
			return ips;
		}catch(final Exception e){
			return ips;
		}
	}

	@Override
	public final boolean unban(final String name){
		try{
			final Object server = Bukkit.getServer();
			final Object playerList = Reflections.getMethod(server.getClass(), "getPlayerList").invoke(server);
			final Object profileBans = Reflections.getMethod(playerList.getClass(), "getProfileBans").invoke(playerList);
			final Object gameProfile = Reflections.getMethod(profileBans.getClass(), "a", String.class).invoke(profileBans, name);
			if(gameProfile == null)
				return false;
			Reflections.getMethod(profileBans.getClass(), "remove", gameProfile.getClass()).invoke(profileBans, gameProfile);
			return true;
		}catch(final Exception ex){
			return false;
		}
	}

	@Override
	public final boolean unbanIp(final String ip){
		try{
			final Matcher match = a.matcher(ip);
			if(!match.matches())
				return false;
			final Object server = Bukkit.getServer();
			final Object playerList = Reflections.getMethod(server.getClass(), "getPlayerList").invoke(server);
			final Object ipBans = Reflections.getMethod(playerList.getClass(), "getIPBans").invoke(playerList);
			Reflections.getMethod(ipBans.getClass(), "remove", String.class).invoke(ipBans, ip);
			return true;
		}catch(final Exception ex){
			return false;
		}
	}

}

