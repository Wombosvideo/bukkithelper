package core.bukkithelper.versionbased.reflected;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import core.bukkithelper.utils.Reflections;
import core.bukkithelper.versionbased.base.EquipmentPacketSender;
import core.bukkithelper.versionbased.versions.ReflectedPacketSender;

public class DefaultEquipmentPacketSender implements EquipmentPacketSender {

	@Override
	public final void fakeEquip(final Player p, final ItemStack[] equipment){
		try {
			final Object packet0 = Reflections.getConstructor(Reflections.getNMSClass("PacketPlayOutEntityEquipment"), int.class, int.class, Reflections.getNMSClass("ItemStack")).newInstance(p.getEntityId(), 0, Reflections.getMethod(Reflections.getCraftClass("inventory.CraftItemStack"), "asNMSCopy", ItemStack.class).invoke(null, equipment[0]));
			final Object packet1 = Reflections.getConstructor(Reflections.getNMSClass("PacketPlayOutEntityEquipment"), int.class, int.class, Reflections.getNMSClass("ItemStack")).newInstance(p.getEntityId(), 1, Reflections.getMethod(Reflections.getCraftClass("inventory.CraftItemStack"), "asNMSCopy", ItemStack.class).invoke(null, equipment[1]));
			final Object packet2 = Reflections.getConstructor(Reflections.getNMSClass("PacketPlayOutEntityEquipment"), int.class, int.class, Reflections.getNMSClass("ItemStack")).newInstance(p.getEntityId(), 2, Reflections.getMethod(Reflections.getCraftClass("inventory.CraftItemStack"), "asNMSCopy", ItemStack.class).invoke(null, equipment[2]));
			final Object packet3 = Reflections.getConstructor(Reflections.getNMSClass("PacketPlayOutEntityEquipment"), int.class, int.class, Reflections.getNMSClass("ItemStack")).newInstance(p.getEntityId(), 3, Reflections.getMethod(Reflections.getCraftClass("inventory.CraftItemStack"), "asNMSCopy", ItemStack.class).invoke(null, equipment[3]));
			final Object packet4 = Reflections.getConstructor(Reflections.getNMSClass("PacketPlayOutEntityEquipment"), int.class, int.class, Reflections.getNMSClass("ItemStack")).newInstance(p.getEntityId(), 4, Reflections.getMethod(Reflections.getCraftClass("inventory.CraftItemStack"), "asNMSCopy", ItemStack.class).invoke(null, equipment[4]));
			ReflectedPacketSender.sSendNotTo(packet0, p);
			ReflectedPacketSender.sSendNotTo(packet1, p);
			ReflectedPacketSender.sSendNotTo(packet2, p);
			ReflectedPacketSender.sSendNotTo(packet3, p);
			ReflectedPacketSender.sSendNotTo(packet4, p);
		} catch (final Exception e) {}
	}

}
