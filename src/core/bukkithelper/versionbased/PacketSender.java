package core.bukkithelper.versionbased;

import java.util.ArrayList;

import org.bukkit.entity.Player;

public abstract class PacketSender {

	public final void sendNotTo(final Object packet, final ArrayList<Player> ps) {
		for(final Player a : VersionBase.getOnlinePlayers())
			if(!ps.contains(a))
				this.sendTo(packet, a);
	}

	public final void sendNotTo(final Object packet, final Player p) {
		final ArrayList<Player> pl = new ArrayList<Player>();
		pl.add(p);
		this.sendNotTo(packet, pl);
	}

	public abstract void sendTo(Object packet, Player p);

	public final void sendToAll(final Object packet) {
		for(final Player p : VersionBase.getOnlinePlayers())
			this.sendTo(packet, p);
	}

}
