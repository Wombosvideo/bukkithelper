package core.bukkithelper.versionbased.base;

import org.bukkit.entity.Player;

public interface PlayerInfoPacketSender {

	public abstract void sendPlayerInfo(Player p, String name);

}
