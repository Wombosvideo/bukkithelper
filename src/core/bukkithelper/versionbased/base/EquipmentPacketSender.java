package core.bukkithelper.versionbased.base;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface EquipmentPacketSender {

	public abstract void fakeEquip(Player p, ItemStack[] equipment);

}
