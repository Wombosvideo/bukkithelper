package core.bukkithelper.versionbased.base;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public interface BanManager {

	public static final Pattern a = Pattern.compile("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

	public abstract boolean ban(String name);

	public abstract boolean ban(String name, Date banned, Date expires, String bannedBy, String reason);

	public abstract boolean banIp(String ban);

	public abstract boolean banIp(String name, String ip, String reason, Date banned, Date expires);

	public abstract List<String> getBans();

	public abstract List<String> getIPBans();

	public abstract boolean unban(String name);

	public abstract boolean unbanIp(String ip);

}
