package core.bukkithelper.signs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.TNTPrimed;

import com.google.common.collect.Lists;

import core.bukkithelper.BukkitHelper;

public class TNTSign {

	public static final ArrayList<TNTSign> SIGNS = Lists.newArrayList();

	public static final void create(final Location location){
		SIGNS.add(new TNTSign(location));
	}

	public static final TNTSign get(final Location loc){
		for(final TNTSign sign : SIGNS)
			if(loc.distance(sign.location) < 1)
				return sign;
		return null;
	}

	public static final void readFromFile() throws IOException{
		final File file = new File(BukkitHelper.SPIGOT_JAR.getParentFile().getAbsoluteFile(), "signs.txt");
		file.getParentFile().mkdirs();
		file.createNewFile();
		final FileReader rd = new FileReader(file);
		final BufferedReader br = new BufferedReader(rd);
		final String line = br.readLine();
		br.close();
		rd.close();
		if(!line.contains("\\;"))
			return;
		for(final String signstring : line.split(";")){
			final String[] locAr = signstring.split(":");
			create(new Location(Bukkit.getWorld(locAr[0]), Integer.parseInt(locAr[1]), Integer.parseInt(locAr[2]), Integer.parseInt(locAr[3])));
		}
	}

	public static final void writeToFile(){
		final StringBuilder builder = new StringBuilder();
		for(final TNTSign sign : SIGNS)
			builder.append(sign.location.getWorld().getName() + ":" + sign.location.getBlockX() + ":" + sign.location.getBlockY() + ":" + sign.location.getBlockZ() + ";");
		PrintWriter w = null;
		try {
			final File file = new File(BukkitHelper.SPIGOT_JAR.getParentFile().getAbsoluteFile(), "signs.txt");
			file.getParentFile().mkdirs();
			w = new PrintWriter(file, "UTF-8");
		} catch (final IOException e) {
			e.printStackTrace();
			return;
		}
		w.println(builder.toString());
		w.close();
	}

	private final Location location;

	private TNTSign(final Location location) {
		this.location = location;
	}

	public final boolean isEnabled(){
		return this.isExisting() && ((Sign)this.location.getBlock().getState()).getLine(1).equals("Enabled");
	}

	public final boolean isExisting(){
		return this.location.getBlock().getState() instanceof Sign && ((Sign)this.location.getBlock().getState()).getLine(0).equals("[TNTSpawner]");
	}

	public final void setEnabled(final boolean enabled){
		if(this.isExisting())
			((Sign)this.location.getBlock().getState()).setLine(1, (enabled ? "En" : "Dis") + "abled");
	}

	public final void spawn(){
		final TNTPrimed tnt = (TNTPrimed) this.location.getWorld().spawnEntity(this.location.clone().add(0.5D, 0D, 0.5D), EntityType.PRIMED_TNT);
		tnt.setIsIncendiary(true);
		tnt.setFuseTicks(100);
	}

}
