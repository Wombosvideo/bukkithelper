package core.bukkithelper.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class PocketWorldEdit {

	public static class WorldEditCubeStructure extends WorldEditStructure {
		public WorldEditCubeStructure() {
			super("cube", false);
		}
		public final ArrayList<Block> generate(final Location center, final int sizeX, final int sizeY, final int sizeZ) {
			final Location edge1 = center.clone().add(Math.floor(sizeX/2D), Math.floor(sizeY/2D), Math.floor(sizeZ/2D));
			final Location edge2 = center.clone().subtract(Math.ceil(sizeX/2D), Math.ceil(sizeY/2D), Math.ceil(sizeZ/2D));
			return this.generate(edge1, edge2);
		}
		public final ArrayList<Block> generate(final Location edge1, final Location edge2){
			final ArrayList<Block> blocks = new ArrayList<Block>();
			for(int x = Math.min(edge1.getBlockX(), edge2.getBlockX()); x <= Math.max(edge1.getBlockX(), edge2.getBlockX()); x++)
				for(int y = Math.min(edge1.getBlockY(), edge2.getBlockY()); y <= Math.max(edge1.getBlockY(), edge2.getBlockY()); y++)
					for(int z = Math.min(edge1.getBlockZ(), edge2.getBlockZ()); z <= Math.max(edge1.getBlockZ(), edge2.getBlockZ()); z++)
						blocks.add(edge1.getWorld().getBlockAt(x, y, z));
			return blocks;
		}
	}

	public static class WorldEditCylinderStructure extends WorldEditStructure {
		public WorldEditCylinderStructure() {
			super("cyl", true);
		}
		public final ArrayList<Block> generate(final Location center, final int radius, final int height, final boolean hollow) {
			final ArrayList<Block> blocks = new ArrayList<Block>();
			final Location edge1 = center.clone().add(radius, height, radius);
			final Location edge2 = center.clone().subtract(radius, 0, radius);
			for(int x = edge2.getBlockX(); x <= edge1.getBlockX(); x++)
				for(int y = edge2.getBlockY(); y <= edge1.getBlockY(); y++)
					for(int z = edge2.getBlockZ(); z <= edge1.getBlockZ(); z++){
						final Block b = edge1.getWorld().getBlockAt(x, y, z);
						if(!hollow & b.getLocation().distanceSquared(center.clone().add(0, y, 0)) <= radius||hollow & (b.getLocation().distanceSquared(center.clone().add(0, y, 0)) == radius || b.getLocation().distanceSquared(center.clone().add(0, y, 0)) == radius-1))
							blocks.add(b);
					}
			return blocks;
		}
	}

	public static class WorldEditSphereStructure extends WorldEditStructure {
		public WorldEditSphereStructure() {
			super("sphere", true);
		}
		public final ArrayList<Block> generate(final Location center, final int radius, final boolean hollow) {
			final ArrayList<Block> blocks = new ArrayList<Block>();
			final Location edge1 = center.clone().add(radius, radius, radius);
			final Location edge2 = center.clone().subtract(radius, radius, radius);
			for(int x = edge2.getBlockX(); x <= edge1.getBlockX(); x++)
				for(int y = edge2.getBlockY(); y <= edge1.getBlockY(); y++)
					for(int z = edge2.getBlockZ(); z <= edge1.getBlockZ(); z++){
						final Block b = edge1.getWorld().getBlockAt(x, y, z);
						if(!hollow && b.getLocation().distanceSquared(center) <= radius||hollow && (b.getLocation().distanceSquared(center) == radius || b.getLocation().distanceSquared(center) == radius-1))
							blocks.add(b);
					}
			return blocks;
		}
	}

	public static abstract class WorldEditStructure {
		private final String name;
		private final boolean canBeHollow;
		private WorldEditStructure(final String name, final boolean canBeHollow){
			this.name = name;
			this.canBeHollow = canBeHollow;
		}
		public final boolean getCanBeHollow(){
			return this.canBeHollow;
		}
		public final String getName(){
			return this.name;
		}
	}

	public static final WorldEditCubeStructure CUBE = new WorldEditCubeStructure();

	public static final WorldEditSphereStructure SPHERE = new WorldEditSphereStructure();

	public static final WorldEditCylinderStructure CYLINDER = new WorldEditCylinderStructure();

	private static final HashMap<String,Location> POS = new HashMap<String,Location>();

	public static final Location getLoc1(final String username){
		return POS.containsKey(username + "__p1") ? POS.get(username + "__p1") : null;
	}

	public static final Location getLoc2(final String username){
		return POS.containsKey(username + "__p2") ? POS.get(username + "__p2") : null;
	}

	public static final Material getMaterialFromItem(Material mat) {
		if(!mat.isBlock())
			switch(mat){
			case BED:
				mat = Material.BED_BLOCK;
			case BREWING_STAND_ITEM:
				mat = Material.BREWING_STAND;
			case CAULDRON_ITEM:
				mat = Material.CAULDRON;
			case DIODE:
				mat = Material.DIODE_BLOCK_OFF;
			case FLOWER_POT_ITEM:
				mat = Material.FLOWER_POT;
			case LAVA_BUCKET:
				mat = Material.LAVA;
			case NETHER_BRICK_ITEM:
				mat = Material.NETHER_BRICK;
			case REDSTONE:
				mat = Material.REDSTONE_WIRE;
			case REDSTONE_COMPARATOR:
				mat = Material.REDSTONE_COMPARATOR_OFF;
			case SIGN:
				mat = Material.SIGN_POST;
			case SUGAR_CANE:
				mat = Material.SUGAR_CANE_BLOCK;
			case WATER_BUCKET:
				mat = Material.WATER;
			default:
				mat = null;
			}
		return mat;
	}

	@SuppressWarnings("unchecked")
	public static final ArrayList<Block> getRandomBlocks(final ArrayList<Block> blocks, final int amount){
		final ArrayList<Block> ourBlocks = (ArrayList<Block>) blocks.clone();
		Collections.shuffle(ourBlocks);
		for(int i = 0; i < amount; i++)
			ourBlocks.remove(i);
		return ourBlocks;
	}

	public static final void setLoc1(final String username, final Location loc){
		POS.put(username + "__p1", loc);
	}

	public static final void setLoc2(final String username, final Location loc){
		POS.put(username + "__p2", loc);
	}

}
