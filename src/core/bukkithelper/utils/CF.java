package core.bukkithelper.utils;

import java.sql.ResultSet;
import java.util.HashMap;

public class CF {

	private static CF ins;
	public static final Object get(final String key){
		return ins.objList.get(key);
	}

	public static final boolean getBoolean(final String key){
		return (boolean) get(key);
	}

	public static final byte getByte(final String key){
		return (byte) get(key);
	}

	public static final double getDouble(final String key){
		return (double) get(key);
	}

	public static final float getFloat(final String key){
		return (float) get(key);
	}

	public static final int getInt(final String key){
		return (int) get(key);
	}

	public static final long getLong(final String key){
		return (long) get(key);
	}

	public static final ResultSet getResultSet(final String key){
		return (ResultSet) get(key);
	}

	public static final short getShort(final String key){
		return (short) get(key);
	}

	public static final String getString(final String key){
		return (String) get(key);
	}

	public static final void put(final String key, final Object value){
		ins.objList.put(key, value);
	}

	public static final void remove(final String key){
		ins.objList.remove(key);
	}

	private final HashMap<String,Object> objList = new HashMap<String,Object>();

}
