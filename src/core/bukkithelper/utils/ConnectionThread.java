package core.bukkithelper.utils;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import core.bukkithelper.BukkitHelper;

public class ConnectionThread implements Runnable{

	int i;
	public ConnectionThread(final int i) {
		this.i = i;
	}

	@Override
	public void run() {
		try{
			final Socket s = new Socket(BukkitHelper.SERVER_IP, this.i);
			BukkitHelper.PORTLIST.add(new Integer(this.i));
			s.close();
		}catch(final UnknownHostException e){

		} catch (final IOException e) {
		}
	}

}
