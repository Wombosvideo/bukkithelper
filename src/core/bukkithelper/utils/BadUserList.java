package core.bukkithelper.utils;

import java.util.ArrayList;
public class BadUserList {

	private static final ArrayList<String> LOCAL = new ArrayList<String>();

	private static final ArrayList<String> MYSQL = new ArrayList<String>();
	private static final ArrayList<String> ADDED = new ArrayList<String>();
	private static final ArrayList<String> REMOV = new ArrayList<String>();
	public static BadUserList ins;
	public static final void add(final boolean local, final String name){if(local)addLocal(name);else addMySQL(name);}

	public static final void addLocal(final String name){if(!containsLocal(name))LOCAL.add(name);}
	public static final void addMySQL(final String name){if(!containsLocal(name))MYSQL.add(name);ADDED.add(name);if(REMOV.contains(name))REMOV.remove(name);}
	public static final boolean contains(final String name){return containsLocal(name) || containsMySQL(name);}

	public static final boolean containsLocal(final String name){return LOCAL.contains(name);}
	public static final boolean containsMySQL(final String name){return MYSQL.contains(name);}
	public static final void remove(final String name){removeLocal(name);removeMySQL(name);}

	public static final void removeLocal(final String name){if(containsLocal(name))LOCAL.remove(name);}
	public static final void removeMySQL(final String name){if(containsMySQL(name))MYSQL.remove(name);if(ADDED.contains(name))ADDED.remove(name);REMOV.add(name);}
	public BadUserList(){
		ins = this;
	}

	@SuppressWarnings("unchecked")
	public void synchronize(){
		final ArrayList<String> ONLINE = MySQL.getBadUsers();
		final ArrayList<String> MYSQL_ = (ArrayList<String>) MYSQL.clone();
		final ArrayList<String> ADDED_ = (ArrayList<String>) ADDED.clone();
		final ArrayList<String> REMOV_ = (ArrayList<String>) REMOV.clone();

		final ArrayList<String> MYSQL_ADD = (ArrayList<String>) ONLINE.clone();
		MYSQL_ADD.removeAll(MYSQL_);
		MYSQL_ADD.removeAll(REMOV_);

		final ArrayList<String> MYSQL_REM = (ArrayList<String>) MYSQL_.clone();
		MYSQL_ADD.removeAll(ONLINE);
		MYSQL_ADD.removeAll(ADDED_);

		final ArrayList<String> ONLIN_ADD = (ArrayList<String>) ADDED_.clone();
		MYSQL_ADD.removeAll(ONLINE);

		final ArrayList<String> ONLIN_REM = (ArrayList<String>) REMOV_.clone();
		MYSQL_ADD.retainAll(ONLINE);

		ADDED.clear();
		REMOV.clear();

		MYSQL.removeAll(MYSQL_REM);
		MYSQL.addAll   (MYSQL_ADD);

		for(final String s : ONLIN_REM)
			MySQL.removeUser(s);
		for(final String s : ONLIN_ADD)
			MySQL.addUser(s);
	}

}
