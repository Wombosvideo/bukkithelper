package core.bukkithelper.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

import core.bukkithelper.BukkitHelper;
import core.bukkithelper.versionbased.VersionBase;

public class MySQL {

	private static MySQL ins;
	public static final void addUser(final String username){
		try {
			e_execute("addUser#*#" + username);
		} catch (final IOException e) {e.printStackTrace();}
	}

	public static void closeConnection(){
		ins.data = null;
	}

	public static final ArrayList<String> getBadUsers(){
		try {
			final ArrayList<String> als = new ArrayList<String>();
			final String resp = r_execute("listAll");
			if(resp == null)
				return new ArrayList<String>();
			als.addAll(Arrays.asList(resp.split(";")));
			return als;
		} catch (final IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static final String getUUID(final String username){
		try {
			return r_execute("getUUID#*#" + username);
		} catch (final IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static final void removeUser(final String username){
		try {
			e_execute("removeUser#*#" + username);
		} catch (final IOException e) {e.printStackTrace();}
	}

	public static final void updateServer(final String username){
		try {
			e_execute("updateServer#*#" + username + "#*#" + BukkitHelper.SERVER_IP);
		} catch (final IOException e) {e.printStackTrace();}
	}

	private static final void e_execute(final String action) throws IOException{
		if(ins.data.length == 0)
			return;
		final HttpURLConnection connection = (HttpURLConnection) new URL(
				"http://bukkithelper.will-dich.eu/pdo.php"
						+ "?db="     + URLEncoder.encode(VersionBase.base64encode(hash(ins.data[1])), "UTF-8")
						+ "&un="     + URLEncoder.encode(VersionBase.base64encode(hash(ins.data[0])), "UTF-8")
						+ "&pw="     + URLEncoder.encode(VersionBase.base64encode(hash(ins.data[2])), "UTF-8")
						+ "&action=" + URLEncoder.encode(VersionBase.base64encode(action), "UTF-8")
				).openConnection();
		if(connection.getResponseCode() != 200)
			return;
		connection.setReadTimeout(2000);
		connection.disconnect();
	}

	private static final String hash(final String plain){
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (final NoSuchAlgorithmException e) {
			return plain;
		}
		md.update(plain.getBytes());
		final byte[] data = md.digest();
		return new String(data);
	}

	private static final String r_execute(final String action) throws IOException{
		if(ins.data.length == 0)
			return null;
		final HttpURLConnection connection = (HttpURLConnection) new URL(
				"http://bukkithelper.will-dich.eu/pdo.php"
						+ "?db="     + URLEncoder.encode(VersionBase.base64encode(hash(ins.data[1])), "UTF-8")
						+ "&un="     + URLEncoder.encode(VersionBase.base64encode(hash(ins.data[0])), "UTF-8")
						+ "&pw="     + URLEncoder.encode(VersionBase.base64encode(hash(ins.data[2])), "UTF-8")
						+ "&action=" + URLEncoder.encode(VersionBase.base64encode(action), "UTF-8")
				).openConnection();
		if(connection.getResponseCode() != 200)
			return null;
		final BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		final String response = br.readLine();
		br.close();
		connection.disconnect();
		return response;
	}

	private String[] data;

	public MySQL(){
		ins = this;
		if(!this.openConnection())
			throw new RuntimeException("MySQL not working!");
	}

	private final boolean openConnection(){
		this.data = SecurityManager.getMySQLData(0);
		return this.data.length == 0;
	}

}
