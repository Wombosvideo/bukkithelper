package core.bukkithelper.utils;
import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Deque;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * @author csterling
 *
 */
public class ZipFileUtil {

	public static String getVer(final ZipFile zipFile) {
		final Enumeration<? extends ZipEntry> files = zipFile.entries();

		while (files.hasMoreElements())
			try {
				final ZipEntry entry = files.nextElement();
				if(entry.getName().startsWith("v1_"))
					return entry.getName();
			} catch (final Exception e) {
				e.printStackTrace();
				continue;
			}
		return "v1_0_R1";
	}

	public static boolean unzipBukkitHelper(final File zipFile, final File outputFile) {
		try {
			return unzipSingle(zipFile, outputFile, "BukkitHelper.jar");
		} catch (final Exception e) {
			e.printStackTrace();
			return false;
		}
		//		Enumeration<? extends ZipEntry> files = zipFile.entries();
		//		File f = null;
		//		FileOutputStream fos = null;
		//
		//		while (files.hasMoreElements()) {
		//			try {
		//				ZipEntry entry = (ZipEntry) files.nextElement();
		//				if(entry.getName().equals("BukkitHelper.jar")){
		//					InputStream eis = zipFile.getInputStream(entry);
		//					byte[] buffer = new byte[1024];
		//					int bytesRead = 0;
		//					f = new File(outputFile.getAbsolutePath());
		//					f.getParentFile().mkdirs();
		//					f.createNewFile();
		//					fos = new FileOutputStream(f);
		//
		//					while ((bytesRead = eis.read(buffer)) != -1) {
		//						fos.write(buffer, 0, bytesRead);
		//					}
		//				}
		//			} catch (IOException e) {
		//				e.printStackTrace();
		//				continue;
		//			} finally {
		//				if (fos != null) {
		//					try {
		//						fos.close();
		//					} catch (IOException e) {
		//						// ignore
		//					}
		//				}
		//			}
		//		}
		//		return true;
	}

	/**
	 * @param zipFile
	 * @param jiniHomeParentDir
	 */
	public static boolean unzipFileIntoDirectory(final ZipFile zipFile, final File jiniHomeParentDir) {
		final Enumeration<? extends ZipEntry> files = zipFile.entries();
		File f = null;
		FileOutputStream fos = null;

		while (files.hasMoreElements())
			try {
				final ZipEntry entry = files.nextElement();
				final InputStream eis = zipFile.getInputStream(entry);
				final byte[] buffer = new byte[1024];
				int bytesRead = 0;

				f = new File(jiniHomeParentDir.getAbsolutePath() + File.separator + entry.getName());

				if (entry.isDirectory()) {
					f.mkdirs();
					continue;
				} else {
					f.getParentFile().mkdirs();
					f.createNewFile();
				}

				fos = new FileOutputStream(f);

				while ((bytesRead = eis.read(buffer)) != -1)
					fos.write(buffer, 0, bytesRead);
			} catch (final IOException e) {
				e.printStackTrace();
				continue;
			} finally {
				if (fos != null)
					try {
						fos.close();
					} catch (final IOException e) {}
			}
		return true;
	}

	public static boolean unzipPluginYML(final File zipFile, final File outputFile) {
		try {
			return unzipSingle(zipFile, outputFile, "plugin.yml");
		} catch (final Exception e) {
			e.printStackTrace();
			return false;
		}
		//		Enumeration<? extends ZipEntry> files = zipFile.entries();
		//		File f = null;
		//		FileOutputStream fos = null;
		//
		//		while (files.hasMoreElements()) {
		//			try {
		//				ZipEntry entry = (ZipEntry) files.nextElement();
		//				if(entry.getName().equals("plugin.yml")){
		//					InputStream eis = zipFile.getInputStream(entry);
		//					byte[] buffer = new byte[1024];
		//					int bytesRead = 0;
		//					f = new File(outputFile.getAbsolutePath());
		//					f.getParentFile().mkdirs();
		//					f.createNewFile();
		//					fos = new FileOutputStream(f);
		//
		//					while ((bytesRead = eis.read(buffer)) != -1) {
		//						fos.write(buffer, 0, bytesRead);
		//					}
		//				}
		//			} catch (IOException e) {
		//				e.printStackTrace();
		//				continue;
		//			} finally {
		//				if (fos != null) {
		//					try {
		//						fos.close();
		//					} catch (IOException e) {
		//						// ignore
		//					}
		//				}
		//			}
		//		}
		//		return true;
	}

	public static boolean unzipSingle(final File zipFile, final File outfile, final String name) throws Exception{
		outfile.getParentFile().mkdirs();
		final OutputStream out = new FileOutputStream(outfile);
		final FileInputStream fin = new FileInputStream(zipFile);
		final BufferedInputStream bin = new BufferedInputStream(fin);
		final ZipInputStream zin = new ZipInputStream(bin);
		ZipEntry ze = null;
		while ((ze = zin.getNextEntry()) != null)
			if (ze.getName().equals(name)) {
				final byte[] buffer = new byte[8192];
				int len;
				while ((len = zin.read(buffer)) != -1)
					out.write(buffer, 0, len);
				out.close();
				break;
			}
		zin.close();
		return true;
	}

	@SuppressWarnings("resource")
	public static void zip(File directory, final File zipfile) throws IOException {
		final URI base = directory.toURI();
		final Deque<File> queue = new LinkedList<File>();
		queue.push(directory);
		final OutputStream out = new FileOutputStream(zipfile);
		Closeable res = out;
		try {
			final ZipOutputStream zout = new ZipOutputStream(out);
			res = zout;
			while (!queue.isEmpty()) {
				directory = queue.pop();
				for (final File kid : directory.listFiles()) {
					String name = base.relativize(kid.toURI()).getPath();
					if (kid.isDirectory()) {
						queue.push(kid);
						name = name.endsWith("/") ? name : name + "/";
						zout.putNextEntry(new ZipEntry(name));
					} else {
						zout.putNextEntry(new ZipEntry(name));
						copy(kid, zout);
						zout.closeEntry();
					}
				}
			}
		} finally {
			res.close();
		}
	}

	/**
	 * Zip up a directory
	 * 
	 * @param directory
	 * @param zipName
	 * @throws IOException
	 */
	public static void zipDir(final String directory, final String zipName) throws IOException {
		// create a ZipOutputStream to zip the data to
		final ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipName));
		final String path = "";
		zipDir(directory, zos, path);
		// close the stream
		zos.close();
	}

	/**
	 * Zip up a directory path
	 * @param directory
	 * @param zos
	 * @param path
	 * @throws IOException
	 */
	public static void zipDir(final String directory, final ZipOutputStream zos, final String path) throws IOException {
		System.out.println("Path: " + path );
		final File zipDir = new File(directory);
		// get a listing of the directory content
		final String[] dirList = zipDir.list();
		final byte[] readBuffer = new byte[2156];
		int bytesIn = 0;
		// loop through dirList, and zip the files
		for (final String element : dirList) {
			final File f = new File(zipDir, element);
			if (f.isDirectory()) {
				final String filePath = f.getPath();
				zipDir(filePath, zos, path + f.getName() + "/");
				continue;
			}
			final FileInputStream fis = new FileInputStream(f);
			try {
				final ZipEntry anEntry = new ZipEntry(path + f.getName());
				zos.putNextEntry(anEntry);
				bytesIn = fis.read(readBuffer);
				while (bytesIn != -1) {
					zos.write(readBuffer, 0, bytesIn);
					bytesIn = fis.read(readBuffer);
				}
			} finally {
				fis.close();
			}
		}
	}

	private static void copy(final File file, final OutputStream out) throws IOException {
		final InputStream in = new FileInputStream(file);
		try {
			copy(in, out);
		} finally {
			in.close();
		}
	}

	@SuppressWarnings("unused")
	private static void copy(final InputStream in, final File file) throws IOException {
		final OutputStream out = new FileOutputStream(file);
		try {
			copy(in, out);
		} finally {
			out.close();
		}
	}

	private static void copy(final InputStream in, final OutputStream out) throws IOException {
		final byte[] buffer = new byte[1024];
		while (true) {
			final int readCount = in.read(buffer);
			if (readCount < 0)
				break;
			out.write(buffer, 0, readCount);
		}
	}

}