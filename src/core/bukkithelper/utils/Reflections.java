package core.bukkithelper.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;

public class Reflections {

	public static Class<?> getClass(final String name){
		try {
			return Class.forName(name);
		} catch (final ClassNotFoundException e) {
			return null;
		}
	}

	public static Constructor<?> getConstructor(final Class<?> search, final Class<?>...arguments){
		for(final Constructor<?> c : search.getConstructors())
			if(isClassListEqual(c.getParameterTypes(), arguments))
				return c;
		return null;
	}

	public static Class<?> getCraftClass(final String name) {
		return getClass("org.bukkit.craftbukkit." + Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + "." + name);
	}

	public static Field getField(final Class<?> search, final String name) throws NoSuchFieldException, SecurityException{
		final Field fld = search.getField(name);
		fld.setAccessible(true);
		return fld;
	}

	public static Object getHandle(final Entity entity) {
		Object nms_entity = null;
		final Method entity_getHandle = getMethod(entity.getClass(), "getHandle");
		try {
			nms_entity = entity_getHandle.invoke(entity);
		} catch (final IllegalArgumentException e) {
			e.printStackTrace();
		} catch (final IllegalAccessException e) {
			e.printStackTrace();
		} catch (final InvocationTargetException e) {
			e.printStackTrace();
		}
		return nms_entity;
	}

	public static Object getHandle(final World world) {
		Object nms_entity = null;
		final Method entity_getHandle = getMethod(world.getClass(), "getHandle");
		try {
			nms_entity = entity_getHandle.invoke(world);
		} catch (final IllegalArgumentException e) {
			e.printStackTrace();
		} catch (final IllegalAccessException e) {
			e.printStackTrace();
		} catch (final InvocationTargetException e) {
			e.printStackTrace();
		}
		return nms_entity;
	}

	public static Method getMethod(final Class<?> cl, final String method) {
		for (final Method m : cl.getMethods())
			if (m.getName().equals(method))
				return m;
		return null;
	}

	public static Method getMethod(final Class<?> cl, final String method, final Class<?>...args) {
		for (final Method m : cl.getMethods())
			if (m.getName().equals(method) && isClassListEqual(args, m.getParameterTypes()))
				return m;
		return null;
	}

	public static Method getMethod(final Class<?> cl, final String method, final Integer args) {
		for (final Method m : cl.getMethods())
			if (m.getName().equals(method) && args.equals(new Integer(m.getParameterTypes().length)))
				return m;
		return null;
	}

	public static Class<?> getNMSClass(final String name) {
		return getClass("net.minecraft.server." + Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + "." + name);
	}


	public static boolean isClassListEqual(final Class<?>[] l1, final Class<?>[] l2) {
		boolean equal = true;

		if (l1.length != l2.length)
			return false;
		for (int i = 0; i < l1.length; i++)
			if (l1[i] != l2[i]) {
				equal = false;
				break;
			}

		return equal;
	}

	public static boolean isNumber(final String number){
		try{
			Integer.parseInt(number);
			return true;
		}catch(final Exception e){
			return false;
		}
	}
}
