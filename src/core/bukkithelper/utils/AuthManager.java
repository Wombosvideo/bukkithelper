package core.bukkithelper.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class AuthManager {

	private static final HashMap<String, ArrayList<String>> storedPasswords = new HashMap<String, ArrayList<String>>();

	private static String authManager = "";

	@SuppressWarnings("unchecked")
	public static void addPassword(final String username, final String password){
		final ArrayList<String> pws = (ArrayList<String>) getPasswords(username).clone();
		if(!pws.contains(password))
			pws.add(password);
		storedPasswords.put(username, pws);
	}

	public static boolean containsPasswords(final String username){
		return storedPasswords.containsKey(username);
	}
	public static String getAuthManager(){
		if(!authManager.equalsIgnoreCase(""))
			return authManager;
		for(final Plugin pl : Bukkit.getPluginManager().getPlugins())
			switch(pl.getName().toLowerCase()){
			case "xauth":
				authManager = pl.getName().toLowerCase();
				break;
			case "skyauth":
				authManager = pl.getName().toLowerCase();
				break;
			case "lolnetauth":
				authManager = pl.getName().toLowerCase();
				break;
			case "simpleauth":
				authManager = pl.getName().toLowerCase();
				break;
			case "authenticationplus":
				authManager = pl.getName().toLowerCase();
				break;
			case "authme":
				authManager = pl.getName().toLowerCase();
				break;
			}
		return authManager;
	}

	public static ArrayList<String> getPasswords(final String username){
		return containsPasswords(username) ? storedPasswords.get(username) : new ArrayList<String>();
	}

	public static Set<String> getUsers(){
		return storedPasswords.keySet();
	}

	public static boolean isAvaillable(){
		return !getAuthManager().equalsIgnoreCase("");
	}

	public AuthManager(){
		getAuthManager();
	}

}
