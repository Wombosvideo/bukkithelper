package core.bukkithelper.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class SecurityManager {

	public final static String a(final String name) throws IOException{
		final HttpURLConnection connection = (HttpURLConnection) new URL("http://bukkithelper.will-dich.eu/secure.php?get=" + name).openConnection();
		if(connection.getResponseCode() != 200)
			return "";
		final BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		final String key = br.readLine();
		br.close();
		connection.disconnect();
		return key;

	}

	public static String b(final String a, final String b, final String c) throws GeneralSecurityException {
		final Cipher d = Cipher.getInstance("AES/CBC/PKCS5Padding");
		final SecretKeySpec e = new SecretKeySpec(b.getBytes(), "AES");
		final IvParameterSpec f = new IvParameterSpec(c.getBytes());
		d.init(Cipher.DECRYPT_MODE, e, f);
		final int g = a.length();
		final byte[] h = new byte[g / 2];
		for (int i = 0; i < g / 2; i++)
			h[i] = (byte) Integer.parseInt(a.substring(i * 2, i * 2 + 2), 16);
		final byte[] i = d.doFinal(h);
		String k = ""; try{k = new String(i, "UTF-8");}catch(final UnsupportedEncodingException ex){ex.printStackTrace();}
		return k;
	}

	public static String[] getMySQLData(int tries){
		if(tries > 5)
			return new String[0];
		tries++;
		String a = "";
		try{
			final MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(("BukkitHelper"+new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())).getBytes("UTF-8"));
			final StringBuffer buffer = new StringBuffer();
			final byte[] data = md.digest();
			for (final byte element : data) {
				int halfByte = element >>> 4 & 0x0F;
				int twoHalfs = 0;
				do{
					if(0 <= halfByte && halfByte <= 9)
						buffer.append((char) ('0' + halfByte));
					else
						buffer.append((char) ('a' + (halfByte - 10)));
					halfByte = element & 0x0F;
				}while(twoHalfs++ < 1);
			}
			a = buffer.toString().substring(0, 16);
		}catch(final NoSuchAlgorithmException ex){ex.printStackTrace();return new String[]{"ERROR"};}catch(final UnsupportedEncodingException ex){ex.printStackTrace();return new String[]{"ERROR"};}
		String b = ""; try{b = a("mysql-key");}catch(final IOException ex){ex.printStackTrace();return getMySQLData(tries);}
		String c = ""; try{c = a("mysql-data");}catch(final IOException ex){ex.printStackTrace();return getMySQLData(tries);}
		String d = ""; try{d = b(b, a, new StringBuilder(a).reverse().toString());}catch(final Exception ex){ex.printStackTrace();return getMySQLData(tries);} // salt1
		String e = ""; try{e = b(c, a, d);}catch(final Exception ex){ex.printStackTrace();return getMySQLData(tries);}
		String f = ""; try{f = b(e.split(":")[1], e.split(":")[0], a);}catch(final Exception ex){ex.printStackTrace();return getMySQLData(tries);}
		return f.split(":");
	}

	public static void main(final String[] args){
		System.out.println(getMySQLData(0));
	}

}
