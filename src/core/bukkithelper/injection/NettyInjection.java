package core.bukkithelper.injection;
import java.util.HashMap;
import java.util.NoSuchElementException;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import net.minecraft.server.v1_8_R3.NetworkManager;

public class NettyInjection {

	public class PacketInjection extends ChannelDuplexHandler {
		public Player player;

		@Override
		public void channelRead(final ChannelHandlerContext ctx, Object msg) throws Exception {
			msg = NettyInjection.this.onPacketIn(this.player, msg);
			super.channelRead(ctx, msg);
		}

		@Override
		public void write(final ChannelHandlerContext ctx, Object msg, final ChannelPromise promise) throws Exception {
			msg = NettyInjection.this.onPacketOut(this.player, msg);
			super.write(ctx, msg, promise);
		}
	}
	private final String handler = "arena_handler";

	private final HashMap<String, Channel> channels = new HashMap<String, Channel>();

	public void inject(final Player player) throws Exception {
		PacketInjection handel = (PacketInjection) this.getChannel(player).pipeline().get(this.handler);
		if(handel == null){
			handel = new PacketInjection();
			handel.player = player;

			final PacketInjection handle = handel;

			final Thread t = new Thread(){
				@Override
				public void run() {
					int tried = 0;
					while(tried < 5000)
						try{
							NettyInjection.this.getChannel(player).pipeline().addBefore("packet_handler", NettyInjection.this.handler, handle);
							System.out.println("Injected " + player.getName() + ". Needed " + tried + " tries!");
							return;
						}catch(final NoSuchElementException e){
							tried ++;
						}catch(final Exception e){
							System.err.println("Error injecting " + player.getName() + ": " + e + ". Detailed error report:");
							e.printStackTrace();
						}
					System.err.println("Error injecting " + player.getName() + ": PacketHandler not found!");
				}
			};
			t.start();
		}
	}

	public Object onPacketIn(final Player sender, final Object packet){ return packet; }

	public Object onPacketOut(final Player target, final Object packet){ return packet; }

	public void uninject(final Player player) throws Exception {
		final PacketInjection handel = (PacketInjection) this.getChannel(player).pipeline().get(this.handler);
		if(handel != null)
			this.getChannel(player).pipeline().remove(this.handler);
	}

	private Channel getChannel(final Player player) throws Exception {
		Channel channel = this.channels.get(player.getName());
		if(channel == null){
			final NetworkManager manager = ((CraftPlayer)player).getHandle().playerConnection.networkManager;
			channel = (Channel)manager.getClass().getDeclaredField("channel").get(manager);
			this.channels.put(player.getName(), channel);
		}
		return channel;
	}
}