package core.bukkithelper.webserver;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.BadShit;


public class HttpRequest implements Runnable {

	final static String CRLF = "\r\n";
	public final HashMap<String, String> handlers = new HashMap<String, String>();
	public final HashMap<String, String> POST = new HashMap<String, String>();
	public final Socket socket;
	public String fileName;
	public RequestType requestType;
	public InputStream instream;
	public DataOutputStream dos;

	public HttpRequest(final Socket socket) {
		this.socket = socket;
	}

	enum RequestType{
		GET, POST, OTHER;
	}



	@Override
	public void run() {
		try{
			this.processRequest();
		}catch(final IOException e){
			if(BadShit.DEBUG)
				e.printStackTrace();
		}

	}

	public void write(final String string){
		try {
			this.dos.writeUTF(string);
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	private final void processRequest() throws IOException{
		BufferedReader br = null;
		this.instream = this.socket.getInputStream();
		this.dos = new DataOutputStream(this.socket.getOutputStream());
		br = new BufferedReader(new InputStreamReader(this.instream));
		String requestLine;
		final String request = requestLine = br.readLine();
		try{
			final StringTokenizer tokens = new StringTokenizer(request);
			tokens.nextToken();
			this.fileName = tokens.nextToken();
		}catch(final NullPointerException e){
			return;
		}
		final StringBuilder raw = new StringBuilder();
		raw.append("" + requestLine);
		this.requestType = request.startsWith("GET") ? RequestType.GET : request.startsWith("POST") ? RequestType.POST : RequestType.OTHER;
		int length = 0;
		while (!(requestLine = br.readLine()).equals("")) {
			raw.append('\n' + requestLine);
			if(this.requestType.equals(RequestType.POST)){
				final String cHead = "Content-Length: ";
				if(requestLine.startsWith(cHead))
					length = Integer.parseInt(requestLine.substring(cHead.length()));
			}
		}
		final StringBuilder body = new StringBuilder();
		if (this.requestType.equals(RequestType.POST)) {
			int c = 0;
			for (int i = 0; i < length; i++) {
				c = br.read();
				body.append((char) c);
			}
		}
		raw.append('\n'+body.toString());
		this.publishProgress(this.requestType, raw.toString());

	}

	private final void publishProgress(final RequestType t, final String...values){
		final String[] ary = values[0].split("\n");
		for(final String s : ary){
			String key = s.split(": ")[0];
			String value = s.replaceAll(key + ": ", "");
			if(ary[ary.length-1].equals(s) && t.equals(RequestType.POST))
				try{
					for(final String y : s.split("&")){
						key = y.split("=")[0];
						value = y.split("=")[1];
						value = this.decodeUri(value);
						this.POST.put(key, value);
					}
				}catch(final Exception e){}
			this.handlers.put(key, value);

		}
	}

	@SuppressWarnings("unused")
	private final String encodeUri(String uri){
		uri = uri.replaceAll("$", "%24");
		uri = uri.replaceAll("+", "%2B");
		uri = uri.replaceAll(",", "%2C");
		uri = uri.replaceAll("/", "%2F");
		uri = uri.replaceAll(":", "%3A");
		uri = uri.replaceAll(";", "%3B");
		uri = uri.replaceAll("@", "%40");

		uri = uri.replaceAll("&", "%26");
		uri = uri.replaceAll("=", "%3D");
		uri = uri.replaceAll("?", "%3F");

		uri = uri.replaceAll(" ", "%20");
		uri = uri.replaceAll("<", "%3C");
		uri = uri.replaceAll(">", "%3E");
		uri = uri.replaceAll("%", "%25");

		return uri;
	}

	private final String decodeUri(String uri){
		uri = uri.replaceAll("%24", "$");
		uri = uri.replaceAll("%2B", "+");
		uri = uri.replaceAll("%2C", ",");
		uri = uri.replaceAll("%2F", "/");
		uri = uri.replaceAll("%3A", ":");
		uri = uri.replaceAll("%3B", ";");
		uri = uri.replaceAll("%40", "@");

		uri = uri.replaceAll("%26", "&");
		uri = uri.replaceAll("%3D", "=");
		uri = uri.replaceAll("%3F", "?");

		uri = uri.replaceAll("%20", " ");
		uri = uri.replaceAll("%3C", "<");
		uri = uri.replaceAll("%3E", ">");
		uri = uri.replaceAll("%25", "%");
		return uri;
	}

}
