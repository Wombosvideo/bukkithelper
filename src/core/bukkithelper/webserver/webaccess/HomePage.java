package core.bukkithelper.webserver.webaccess;

import org.bukkit.Bukkit;

import core.bukkithelper.versionbased.VersionBase;
import core.bukkithelper.webserver.elements.WebHead.DefaultWebHead;
import core.bukkithelper.webserver.elements.WebPage;

public class HomePage extends WebPage {

	public HomePage() {
		super("/", "Home", "home", false);
		this.setWebHead(new DefaultWebHead(this));
	}

	@Override
	public void loadContent() {
		this.addLine("<div class=\"row\">");
		this.addLine("  <div class=\"col-xs-12 col-md-6 col-lg-3\">");
		this.addLine("    <div class=\"panel panel-blue panel-widget \">");
		this.addLine("      <div class=\"row no-padding\">");
		this.addLine("        <div class=\"col-sm-3 col-lg-5 widget-left\">");
		this.addLine("          <em class=\"glyphicon glyphicon-shopping-user glyphicon-l\"></em>");
		this.addLine("        </div>");
		this.addLine("        <div class=\"col-sm-9 col-lg-7 widget-right\">");
		this.addLine("          <div class=\"large\">" + VersionBase.getOnlinePlayers().size() + " / " + Bukkit.getMaxPlayers() + "</div>");
		this.addLine("          <div class=\"text-muted\">Online Players</div>");
	}

}
