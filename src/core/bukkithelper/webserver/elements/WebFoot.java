package core.bukkithelper.webserver.elements;


public abstract class WebFoot {

	private final WebPage source;

	public WebFoot(final WebPage source) {
		if(source == null)
			throw new RuntimeException("Sorry, but the given argument is null");
		this.source = source;
	}

	public final void _loadContent(){
		this._loadDefaults();
		this.loadContent();
	}

	public final void addLine(final String string){
		this.source.addLine("  " + string);
	}

	public final void addScript(final String name){
		this.source.addLine("<script src=\"" + name + "\"></script>");
	}

	public abstract void loadContent();

	private final void _loadDefaults(){
		this.addScript("jquery-1.11.1.min.js");
		this.addScript("bootstrap.min.js");
		this.addScript("chart.min.js");
		this.addScript("chart-data.js");
		this.addScript("easypiechart.js");
		this.addScript("easypiechart-data.js");
		this.addScript("bootstrap-datepicker.js");
	}

}
