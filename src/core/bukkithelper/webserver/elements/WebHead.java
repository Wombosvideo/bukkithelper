package core.bukkithelper.webserver.elements;


public abstract class WebHead {

	public static class DefaultWebHead extends WebHead {

		public DefaultWebHead(final WebPage source) {
			super(source);
		}

		@Override
		public final void loadContent() {}

	}

	private final WebPage source;

	public WebHead(final WebPage source) {
		if(source == null)
			throw new RuntimeException("Sorry, but the given argument is null");
		this.source = source;
	}

	public final void _loadContent(){
		this.addLine("<head>");
		this._loadDefaults();
		this.loadContent();
		this.addLine("</head>");
	}

	public final void addLine(final String string){
		this.source.addLine(string);
	}

	public final void addStyle(final String name){
		this.source.addLine("<link rel=\"stylesheet\" href=\"" + name + "\" />");
	}

	public abstract void loadContent();

	private final void _loadDefaults(){
		this.addLine("<meta charset=\"utf-8\" />");
		this.addLine("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />");
		this.addLine("<title>BukkitHelper - " + this.source.getMenuText() + "</title>");
		this.addLine("<!--[if lt IE 9]>");
		this.addLine("<script src=\"js/html5shiv.js\"></script>");
		this.addLine("<script src=\"js/respond.min.js\"></script>");
		this.addLine("<![endif]-->");
		this.addStyle("bootstrap.min.css");
		this.addStyle("datepicker3.css");
		this.addStyle("styles.css");
	}

}
