package core.bukkithelper.webserver.elements;

public abstract class WebPage extends WebSite {

	public final class WebFolder extends WebPage {

		public final String[] subpages;

		public WebFolder(final String url, final String menuText, final String menuIcon, final String...subpages) {
			super(url, menuText, menuIcon, true, "404 Not Found");
			this.subpages = subpages;
		}

		@Override
		public final void loadContent() {}

	}

	private final String menuText, menuIcon;
	private WebHead head;
	private WebFoot foot;
	private final boolean folder;

	public WebPage(final String url, final String menuText, final String menuIcon) {
		this(url, menuText, menuIcon, false);
	}

	public WebPage(final String url, final String menuText, final String menuIcon, final boolean folder){
		this(url, menuText, menuIcon, folder, "200 OK");
	}

	public WebPage(final String url, final String menuText, final String menuIcon, final boolean folder, final String responseCode) {
		super(url, "text/html", responseCode, false);
		this.menuText = menuText;
		this.menuIcon = menuIcon;
		this.folder = folder;
	}

	public final String getMenuIcon(){
		return this.menuIcon;
	}

	public final String getMenuText(){
		return this.menuText;
	}

	public final WebFoot getWebFoot(){
		return this.foot;
	}

	public final WebHead getWebHead(){
		return this.head;
	}

	public final boolean isFolder(){
		return this.folder;
	}

	public final void setWebFoot(final WebFoot foot){
		this.foot = foot;
	}

	public final void setWebHead(final WebHead head){
		this.head = head;
	}

}
