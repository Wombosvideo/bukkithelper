package core.bukkithelper.webserver.elements;

import java.util.HashMap;

import org.BadShit;

import core.bukkithelper.utils.Reflections;
import core.bukkithelper.webserver.HttpRequest;
import core.bukkithelper.webserver.WebServer;
import core.bukkithelper.webserver.elements.WebPage.WebFolder;

public abstract class WebSite {

	public final HashMap<String,String> headers = new HashMap<String,String>();
	public final String NL = "\n", CRNL = "\r" + this.NL;
	private String content = "";
	private int indent = 0;
	private final String url, mime, responseCode;
	private final boolean download;
	private int id = 0;

	public WebSite(final String url, final String mime, final String responseCode, final boolean download) {
		this.url = url;
		this.mime = mime;
		this.responseCode = responseCode;
		this.download = download;
	}

	public final void _loadContent(final HttpRequest request){
		if(this instanceof WebPage){
			final WebPage page = (WebPage) this;
			page.getWebHead()._loadContent();
			page.addLine("<!DOCTYPE html>");
			page.addLine("<html>");
			page.addLine("<body>");
			page.addLine("  <div id=\"sidebar-collapse\" class=\"col-sm-3 col-lg-2 sidebar\">");
			page.addLine("    <ul class=\"nav menu\">");
			for(final String classpath : WebServer.pages){
				if(classpath.equals("divider")){
					page.addLine("      <li role=\"presentation\" class=\"divider\"></li>");
					continue;
				}
				WebSite site;
				try {
					site = (WebSite) Reflections.getConstructor(Reflections.getClass(classpath), new Class<?>[0]).newInstance(new Object[0]);
				} catch (final Exception e) {
					if(BadShit.DEBUG)
						e.printStackTrace();
					continue;
				}
				if(site instanceof WebFolder)
					this._listFolder(request, (WebFolder) site, "      ");
				else if(site instanceof WebPage){
					final WebPage loaded = (WebPage) site;
					page.addLine("      <li" + (site.url.equals(this.url) ? " class=\"active\"" : "") + "><a href=\"" + site.url + "\"><span class=\"" + loaded.getMenuIcon() + "\"></span> " + loaded.getMenuText() + "</a></li>");
				}
			}
			page.addLine("    </ul>");
			page.addLine("  </div>");
			page.addLine("  <div class=\"col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main\">");
			page.addLine("    <div class=\"row\">");
			page.addLine("      <div class=\"col-lg-12\">");
			page.addLine("        <h1 class=\"page-header\">" + page.getMenuText() + "</h1>");
			page.addLine("      </div>");
			page.setIndent(3);
			page.loadContent();
			page.setIndent(0);
			page.addLine("    </div>");
			page.addLine("  </div>");
			page.getWebFoot()._loadContent();
			page.addLine("</body>");
			page.addLine("</html>");
		} else
			this.loadContent();
		request.write(this.content);
	}

	public final void addLine(final String string){
		this.content = this.content == "" ? string : this.content + this.NL + (this.indent == 0 ? "" : this.indent()) + string;
	}

	public final int getIndent(){
		return this.indent;
	}

	public final String getMime(){
		return this.mime;
	}

	public final String getResponseCode(){
		return this.responseCode;
	}

	public final String getUrl(){
		return this.url;
	}

	public final String indent(){
		return new String(new char[this.indent * 2]).replace('\0', ' ');
	}

	public final boolean isDownload(){
		return this.download;
	}

	public abstract void loadContent();

	public final void loadHeaders(final HttpRequest request){
		request.write("HTTP/1.1 " + this.responseCode + this.CRNL);
		request.write("Content-type: " + this.mime);
		request.write("Content-Disposition: " + (this.download ? "attachment" : "inline") + ";filename=" + (this instanceof WebFile ? ((WebFile)this).getFileName() : "index.html"));
		for(final String header : this.headers.keySet())
			request.write(header + ": " + this.headers.get(header) + this.CRNL);
	}

	public final void setIndent(final int indent){
		this.indent = indent;
	}

	private final void _listFolder(final HttpRequest request, final WebFolder folder, final String root){
		final WebPage page = (WebPage) this;
		final int id = this.nextId();
		page.addLine(root + "<li class=\"parent \">");
		page.addLine(root + "  <a href=\"#\"><span class=\"" + folder.getMenuIcon() + "\"></span> " + folder.getMenuText() + " <span data-toggle=\"collapse\" href=\"#sub-item-" + id + "\" class=\"icon pull-right\"><em class=\"glyphicon glyphicon-s glyphicon-plus\"></em></span></a>");
		page.addLine(root + "  <ul class=\"children collapse\" id=\"sub-item-" + id + "\">");
		for(final String classpath : folder.subpages){
			if(classpath.equals("divider")){
				page.addLine(root + "    <li role=\"presentation\" class=\"divider\"></li>");
				continue;
			}
			WebSite site;
			try {
				site = (WebSite) Reflections.getConstructor(Reflections.getClass(classpath), new Class<?>[0]).newInstance(new Object[0]);
			} catch (final Exception e) {
				if(BadShit.DEBUG)
					e.printStackTrace();
				continue;
			}
			if(site instanceof WebFolder)
				this._listFolder(request, (WebFolder) site, root + "    ");
			else if(site instanceof WebPage){
				final WebPage loaded = (WebPage) site;
				page.addLine(root + "    <li" + (site.url.equals(this.url) ? " class=\"active\"" : "") + "><a href=\"" + site.url + "\"><span class=\"" + loaded.getMenuIcon() + "\"></span> " + loaded.getMenuText() + "</a></li>");
			}
		}
		page.addLine(root + "  </ul>");
	}

	private final int nextId(){
		this.id ++;
		return this.id;
	}

}
