package core.bukkithelper.webserver.elements;

import core.bukkithelper.versionbased.VersionBase;

public abstract class WebFile extends WebSite {

	private final String base64content, fileName;

	public WebFile(final String url, final String mime, final String base64content, final boolean download, final boolean binary) {
		super(url, mime, "200 OK", download);
		this.fileName = url.split("/")[url.split("/").length - 1] ;
		this.base64content = base64content;
		if(binary)
			this.headers.put("Content-Transfer-Encoding", "binary");
		this.headers.put("Pragma", "no-cache");
	}

	public final String getFileName(){
		return this.fileName;
	}

	@Override
	public final void loadContent() {
		this.addLine(VersionBase.base64decode(this.base64content));
	}

}
