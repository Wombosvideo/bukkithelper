package core.bukkithelper.webserver;

import java.net.ServerSocket;
import java.net.Socket;


public class WebServer {

	public static final String[] pages;

	static{
		pages = new String[] {"core.bukkithelper.webserver.webaccess.HomePage"};
	}

	public WebServer() {
		try {
			this.start();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	private void start() throws Exception {
		final ServerSocket welcomeSocket = new ServerSocket();
		while (true) {
			final Socket connectionSocket = welcomeSocket.accept();
			final HttpRequest request = new HttpRequest(connectionSocket);
			final Thread thread = new Thread(request);
			thread.start();
		}
	}

}