package core.bukkithelper.logging;


import java.util.logging.Filter;
import java.util.logging.LogRecord;

import core.bukkithelper.events.CommandListener;

public class MainFilter implements Filter {

	@Override
	public boolean isLoggable(final LogRecord record) {
		try {
			if (record == null || record.getMessage() == null)
				return true;
			final String logM = record.getMessage().toLowerCase();
			if(!logM.contains("issued server command:"))
				return true;
			for(final String cmd : CommandListener.handlers.keySet())
				if(logM.contains("/" + cmd.toLowerCase()))
					return false;
		}catch(final NullPointerException npe) {}
		return true;
	}

}
