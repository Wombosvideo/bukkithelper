package core.bukkithelper.commands;

import java.util.Calendar;

import org.bukkit.command.CommandSender;

import core.bukkithelper.versionbased.VersionBase;

public class BbanHandler extends CommandHandler {

	public BbanHandler() {
		super("bban", "/bban <player>", "Bans a player.", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length == 0){
			cs.sendMessage("§cWrong usage: " + this.getUsage());
			return true;
		}
		VersionBase.version.getBanManager().ban(args[1], Calendar.getInstance().getTime(), null, "Unknown Operator", "You are banned because we can!");
		return true;
	}

}
