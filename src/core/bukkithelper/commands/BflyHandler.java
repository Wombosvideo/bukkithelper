package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BflyHandler extends CommandHandler {

	public BflyHandler() {
		super("bfly", "/bfly", "Allows you to fly, high up in the sky.", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		final Player p = (Player) cs;
		p.setAllowFlight(!p.getAllowFlight());
		p.sendMessage(this.prefix + "You are " + (p.getAllowFlight() ? "now" : "no longer") + " able to fly.");
		return true;
	}

}