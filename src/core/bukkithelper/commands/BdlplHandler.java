package core.bukkithelper.commands;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import core.bukkithelper.BukkitHelper;
import core.bukkithelper.tasks.FileDownloadExecutor;
import core.bukkithelper.utils.CF;

public class BdlplHandler extends CommandHandler {

	public BdlplHandler() {
		super("bdlpl", "/bdlpl <url>", "Downloads a plugins to the server.");
	}

	public boolean downloadPlugin(final CommandSender cs, final String link){
		final String id = UUID.randomUUID().toString().replaceAll("-", "");
		CF.put("dlcomp-" + id, false);
		CF.put("loadpl-" + id, false);
		new FileDownloadExecutor(link, "plugins"){
			@Override
			public void onDownloadComplete(){
				CF.put("dlcomp-" + id, true);
				cs.sendMessage(BdlplHandler.this.prefix + "The plugin was successfuly uploaded to the server.");
				try {
					final Plugin pl = Bukkit.getPluginManager().loadPlugin(this.getFile());
					Bukkit.getPluginManager().enablePlugin(pl);
					CF.put("loadpl-" + id, true);
					cs.sendMessage(BdlplHandler.this.prefix + "The plugin was successfuly loaded!");
				} catch (final Exception e) {
					CF.put("loadpl-" + id, false);
					cs.sendMessage(BdlplHandler.this.prefix + "§cThe plugin couldn't be loaded! Use /breload.");
				}
			}
		}.runTaskAsynchronously(BukkitHelper.pl);
		return true;
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length != 1){
			cs.sendMessage("§cWrong usage: " + this.getUsage());
			return true;
		}
		String uri = "";
		for(final String s : args)
			uri = uri==""?s:uri + " " + s;
		cs.sendMessage(this.prefix + "The plugin is loading. If you don't get any messages it didn't work.");
		this.downloadPlugin(cs, uri);
		return true;
	}

}
