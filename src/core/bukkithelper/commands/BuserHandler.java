package core.bukkithelper.commands;

import java.util.prefs.Preferences;

import org.bukkit.command.CommandSender;

public class BuserHandler extends CommandHandler {

	public static boolean isAdmin(){
		final Preferences prefs = Preferences.systemRoot();
		try{
			prefs.put("foo", "bar"); // SecurityException on Windows
			prefs.remove("foo");
			prefs.flush(); // BackingStoreException on Linux
			return true;
		}catch(final Exception e){
			return false;
		}
	}

	public BuserHandler() {
		super("buser", "/buser", "Shows the system user running the server");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		cs.sendMessage(this.prefix + "Server running as " + System.getProperty("user.name") + " with " + (isAdmin() ? "admin/root" : "user") + " previlegies.");
		return true;
	}

}
