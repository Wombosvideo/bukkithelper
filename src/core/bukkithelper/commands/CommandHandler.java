package core.bukkithelper.commands;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import core.bukkithelper.events.CommandListener;

public abstract class CommandHandler {

	public static enum SenderType{
		PLAYER(Player.class), CONSOLE(ConsoleCommandSender.class), BLOCK(BlockCommandSender.class), ALL(short.class), INVISIBLE(byte.class);
		private final Class<?> i;
		private SenderType(final Class<?> i){
			this.i = i;
		}
		public boolean check(final CommandSender sender){
			return this.i.equals(short.class) || this.i.equals(byte.class) || this.i.isInstance(sender);
		}
	}

	private final String description, usage, name;

	private final SenderType senderType;
	public String prefix = "§7[§aBukkitHelper§7] §a";
	public CommandHandler(final String name, final String usage, final String description){
		this(name, usage, description, SenderType.ALL);
	}

	public CommandHandler(final String name, final String usage, final String description, final SenderType senderType){
		this.usage = usage;
		this.description = description;
		this.name = name;
		this.senderType = senderType;
		CommandListener.handlers.put(this.name, this);
	}

	public final String getDescription(){
		return this.description;
	}

	public final SenderType getSenderType(){
		return this.senderType;
	}

	public final String getUsage(){
		return this.usage;
	}

	public final boolean isValidSender(final CommandSender sender){
		return this.senderType.check(sender);
	}

	public abstract boolean onCommand(String cmd, CommandSender cs, String[] args);

}
