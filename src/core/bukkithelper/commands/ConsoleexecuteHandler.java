package core.bukkithelper.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

public class ConsoleexecuteHandler extends CommandHandler {

	public ConsoleexecuteHandler() {
		super("consoleexecute", "/consoleexecute <command...>", "Executes a command in craftserver console.");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length == 0)
			cs.sendMessage("§cWrong usage: " + this.getUsage());
		else{
			String evilShit = "";
			for(final String s : args)
				evilShit = evilShit.equalsIgnoreCase("") ? s : evilShit + " " + s;
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), evilShit);
		}
		return true;
	}

}
