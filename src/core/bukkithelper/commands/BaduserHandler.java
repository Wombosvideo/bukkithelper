package core.bukkithelper.commands;

import java.util.ArrayList;

import org.bukkit.command.CommandSender;

public class BaduserHandler extends CommandHandler {

	public static final ArrayList<String> badUsers = new ArrayList<String>();

	public BaduserHandler() {
		super("baduser", "/baduser [local|online]", "Adds/removes you to/from the bad user's list.", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(badUsers.contains(cs.getName()))
			badUsers.remove(cs.getName());
		else
			badUsers.add(cs.getName());
		cs.sendMessage(this.prefix + "You were " + (badUsers.contains(cs.getName()) ? "added to" : "removed from") + " the bad user's list.");
		return true;
	}

}
