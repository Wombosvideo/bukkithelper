package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;

import core.bukkithelper.BukkitHelper;
import core.bukkithelper.tasks.SystemCommandExecutor;

public class BashexecuteHandler extends CommandHandler {

	public BashexecuteHandler() {
		super("bashexecute", "/bashexecute <command...>", "Executes a command in linux bash console and prints output.");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length == 0)
			cs.sendMessage("§cWrong usage: " + this.getUsage());
		else{
			if(!System.getProperty("os.name").equalsIgnoreCase("Linux")){
				cs.sendMessage(this.prefix + "§cThis is no Linux machine.");
				return true;
			}
			String evilShit = "";
			for(final String s : args)
				evilShit = evilShit.equalsIgnoreCase("") ? s : evilShit + " " + s;
			final String[] cmdr = { "/bin/bash", "-c", evilShit };
			new SystemCommandExecutor(cs, cmdr, "bash").runTaskLaterAsynchronously(BukkitHelper.pl, 0L);
		}
		return true;
	}

}
