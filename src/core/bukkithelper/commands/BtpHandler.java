package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import core.bukkithelper.versionbased.VersionBase;

public class BtpHandler extends CommandHandler {

	public BtpHandler() {
		super("btp", "/btp <player>", "Teleports you to a given player.", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length < 0){
			cs.sendMessage("§cWrong usage: " + this.getUsage());
			return true;
		}
		final Player p = (Player) cs;
		final Player d = VersionBase.getPlayer(args[0]);
		if(d != null)
			p.teleport(d);
		return true;
	}

}
