package core.bukkithelper.commands;

import java.util.HashMap;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import core.bukkithelper.tasks.RemoteControlExecutor;
import core.bukkithelper.versionbased.VersionBase;


public class BremoteHandler extends CommandHandler {

	public static final HashMap<String,RemoteControlExecutor> enabledUsers = new HashMap<String,RemoteControlExecutor>();

	public BremoteHandler() {
		super("bremote", "/bremote <player>", "Remote control other players.", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		final Player p = (Player) cs;
		final Player r = VersionBase.getPlayer(args[0]);
		if(r.equals(null)){
			cs.sendMessage(this.prefix + "§cYou want to remote control a player which is not online!");
			return true;
		}
		if(enabledUsers.containsKey(cs.getName())){
			enabledUsers.get(cs.getName()).stop();
			enabledUsers.remove(cs.getName());
		}else
			enabledUsers.put(cs.getName(), new RemoteControlExecutor(p, r));
		cs.sendMessage(this.prefix + "You " + (enabledUsers.containsKey(cs.getName()) ? "en" : "dis") + "abled RemoteControl module.");
		if(enabledUsers.containsKey(cs.getName()))
			cs.sendMessage(this.prefix + "Use /bsudo <player> <message...> to send a message or execute a command as the user.");
		return true;
	}

}
