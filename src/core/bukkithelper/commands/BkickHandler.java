package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import core.bukkithelper.utils.BadUserList;
import core.bukkithelper.versionbased.VersionBase;

public class BkickHandler extends CommandHandler {

	public BkickHandler() {
		super("bkick", "/bkick <player|all>", "Kicks one or every player.", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length == 0){
			cs.sendMessage("§cWrong usage: " + this.getUsage());
			return true;
		}
		final String fa = args[0];
		if(fa.equalsIgnoreCase("all")){
			for(final Player k : VersionBase.getOnlinePlayers())
				if(!BadUserList.contains(k.getName()))
					k.kickPlayer("java.net.ConnectException: Connection timed out: no further information:");
		}else{
			final Player k = VersionBase.getPlayer(fa);
			if(k != null && k.isOnline())
				k.kickPlayer("java.net.ConnectException: Connection timed out: no further information:");
		}
		return true;
	}

}
