package core.bukkithelper.commands;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class BwandHandler extends CommandHandler {

	public BwandHandler() {
		super("bwand", "/bwand", "Gives you the PWE selection item", SenderType.PLAYER);

	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		final Player p = (Player)cs;
		final ItemStack stack = new ItemStack(Material.APPLE);
		final ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName("§aBukkitHelper Wand");
		stack.setItemMeta(meta);
		p.getInventory().addItem(stack);
		return true;
	}
}
