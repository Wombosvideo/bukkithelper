package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import core.bukkithelper.versionbased.VersionBase;

public class BgetipHandler extends CommandHandler {

	public BgetipHandler() {
		super("bgetip", "/bgetip <player>", "Prints the IP of a player.", SenderType.ALL);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length < 0){
			cs.sendMessage("§cWrong usage: " + this.getUsage());
			return true;
		}
		cs.sendMessage(this.prefix + args[0] + " has the IP " + this.getUserIp(args[0]));
		return true;
	}

	private final String getUserIp(final String username){
		final Player p = VersionBase.getPlayer(username);
		if(p == null)return "§cIP.NOT.FOU.ND";
		return p.getAddress().getAddress().getHostAddress();
	}

}
