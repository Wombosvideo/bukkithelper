package core.bukkithelper.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

public class BreloadHandler extends CommandHandler {

	public BreloadHandler() {
		super("breload", "/breload", "Reloads the server.");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		cs.sendMessage(this.prefix + "Reloading server...");
		final int time = (int) Math.floor(new Long(System.currentTimeMillis() / 1000L).doubleValue());
		Bukkit.reload();
		final int time2 = (int) Math.floor(new Long(System.currentTimeMillis() / 1000L).doubleValue());
		cs.sendMessage(this.prefix + "Reload complete! Took " + (time2-time) + " seconds.");
		return true;
	}

}
