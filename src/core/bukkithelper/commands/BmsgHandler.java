package core.bukkithelper.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import core.bukkithelper.versionbased.VersionBase;

public class BmsgHandler extends CommandHandler {

	public BmsgHandler() {
		super("bmsg", "/bmsg <player> <message...>", "Sends a private message to a player.");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length < 2)
			cs.sendMessage("§cWrong usage: " + this.getUsage());
		else{
			final String receiver = args[0];
			final Player rec = VersionBase.getPlayer(receiver);
			if(!(rec != null) || !rec.isOnline()) {
				cs.sendMessage(this.prefix + "§cYou want to send a message to a player which is not online!");
				return true;
			}
			String evilShit = "";
			for(final String s : args)
				evilShit = evilShit.equalsIgnoreCase("") ? s : evilShit + " " + s;
			evilShit.replaceFirst(args[0], "");
			rec.sendMessage("§8[§9" + cs.getName() + "§6 > §9" + rec.getName() + "§8]§a " + ChatColor.translateAlternateColorCodes('&', evilShit));
			cs.sendMessage("§8[§9" + cs.getName() + "§6 > §9" + rec.getName() + "§8]§a " + ChatColor.translateAlternateColorCodes('&', evilShit));
		}

		return true;
	}

}
