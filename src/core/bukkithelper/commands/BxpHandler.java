package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BxpHandler extends CommandHandler {

	public BxpHandler() {
		super("bxp", "/bxp add <levels> OR /bxp remove <levels> OR /bxp set <levels>", "Gives, removes or sets experience levels");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(!(cs instanceof Player)){
			cs.sendMessage(this.prefix + "You have to be a player to execute this command.");
			return true;
		}
		final Player p = (Player)cs;
		if(args.length == 0)
			cs.sendMessage("§cWrong usage: " + this.getUsage());
		else if(args[0].equalsIgnoreCase("add"))
			if(this.validate(args)){
				p.setLevel(p.getLevel() + Integer.parseInt(args[1]));
				p.sendMessage(this.prefix + "Your experience level was set to " + p.getLevel());
			}else
				cs.sendMessage("§cWrong usage: " + this.getUsage());
		else if(args[0].equalsIgnoreCase("remove"))
			if(this.validate(args)){
				p.setLevel(p.getLevel() - Integer.parseInt(args[1]));
				p.sendMessage(this.prefix + "Your experience level was set to " + p.getLevel());
			}else
				cs.sendMessage("§cWrong usage: " + this.getUsage());
		else if(args[0].equalsIgnoreCase("set"))
			if(this.validate(args)){
				p.setLevel(Integer.parseInt(args[1]));
				p.sendMessage(this.prefix + "Your experience level was set to " + p.getLevel());
			}else
				cs.sendMessage("§cWrong usage: " + this.getUsage());
		return true;
	}

	boolean validate(final String[] s){
		@SuppressWarnings("unused")
		int i = 0;
		try{
			i = Integer.parseInt(s[1]);
			return true;
		}catch(final Exception ex){
			return false;
		}
	}

}
