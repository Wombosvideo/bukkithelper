package core.bukkithelper.commands;

import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import core.bukkithelper.versionbased.VersionBase;

public class BgmHandler extends CommandHandler {

	public BgmHandler() {
		super("bgm", "/bgm <0|1|2|3>", "Change your gamemode", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length == 0 || args.length > 1 || !this.cast(args[0]) || !this.valid(this.parse(args[0])))
			cs.sendMessage("§cWrong usage: " + this.getUsage());
		else{
			final Player p = (Player) cs;
			final GameMode m = this.parse(args[0]);
			if(!p.getGameMode().equals(m)){
				p.setGameMode(m);
				p.sendMessage(this.prefix + "Your gamemode was changed to \"" + m.name().toLowerCase() + "\"");
			}
		}
		return true;
	}

	private boolean cast(final String i){
		try{
			Integer.parseInt(i);
			return true;
		}catch(final Exception ex){
			return false;
		}
	}

	private GameMode parse(final String i){
		try{
			final int m = Integer.parseInt(i);
			if(0 > m || m > 3)
				return null;
			else
				return GameMode.getByValue(m);
		}catch(final Exception ex){
			return null;
		}
	}

	private boolean valid(final GameMode mode){
		// mode == null																						-> false
		// mode != null && mode.getValue() == 3 && !VersionBase.version.isSpectatorModeSupported()			-> false
		// mode != null && (mode.getValue() != 3 || VersionBase.version.isSpectatorModeSupported())			-> true
		if(mode != null)
			if(mode.getValue() == 3 && !VersionBase.version.isSpectatorModeSupported())
				return false;
			else
				return true;
		else
			return false;
	}

}
