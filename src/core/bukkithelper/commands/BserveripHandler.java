package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;

import core.bukkithelper.BukkitHelper;

public class BserveripHandler extends CommandHandler {

	public BserveripHandler() {
		super("bserverip", "/bserverip", "Outputs the server ip and its port.");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		cs.sendMessage(this.prefix + "This server is running on " + BukkitHelper.SERVER_IP);
		return true;
	}

}
