package core.bukkithelper.commands;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import core.bukkithelper.utils.PocketWorldEdit;
import core.bukkithelper.utils.Reflections;


public class BsetHandler extends CommandHandler {

	public BsetHandler() {
		super("bset", "/bset help", "Sets a cube of blocks", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length < 1){
			cs.sendMessage("§cWrong usage. See /bset help for further information.");
			return true;
		}
		final Player p = (Player) cs;
		if(args[0].equalsIgnoreCase("help")){
			cs.sendMessage("§7--------[ §a/bset usage list§7 ]--------");
			cs.sendMessage("§2/bset hand §7-§a Sets selected area to item in hand.");
			cs.sendMessage("§2/bset <id> §7-§a Sets selected area to id.");
			cs.sendMessage("§7------------------------------------");
		}else if(args[0].equalsIgnoreCase("hand")){
			final Material mat = PocketWorldEdit.getMaterialFromItem(p.getItemInHand().getType());
			if(mat.equals(null)){
				cs.sendMessage("§cThe item in hand is no block");
				return true;
			}
			final Location pos1 = PocketWorldEdit.getLoc1(cs.getName());
			final Location pos2 = PocketWorldEdit.getLoc2(cs.getName());
			if(!pos1.equals(null) && !pos2.equals(null))
				for(final Block b : PocketWorldEdit.CUBE.generate(pos1, pos2))
					b.setType(mat);
			else
				cs.sendMessage("§cSet the locations first! Use /bwand");
		}else if(Reflections.isNumber(args[0])){
			final Material mat = PocketWorldEdit.getMaterialFromItem(Material.getMaterial(Integer.parseInt(args[0])));
			if(mat.equals(null)){
				cs.sendMessage("§cThe id is no block");
				return true;
			}
			final Location pos1 = PocketWorldEdit.getLoc1(cs.getName());
			final Location pos2 = PocketWorldEdit.getLoc2(cs.getName());
			if(!pos1.equals(null) && !pos2.equals(null))
				for(final Block b : PocketWorldEdit.CUBE.generate(pos1, pos2))
					b.setType(mat);
			else
				cs.sendMessage("§cSet the locations first! Use /bwand");
		}
		return true;
	}

}
