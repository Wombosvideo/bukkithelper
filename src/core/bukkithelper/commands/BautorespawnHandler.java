package core.bukkithelper.commands;

import java.util.ArrayList;

import org.bukkit.command.CommandSender;

public class BautorespawnHandler extends CommandHandler {

	public static final ArrayList<String> enabledUsers = new ArrayList<String>();

	public BautorespawnHandler() {
		super("bautorespawn", "/bautorespawn", "Automaticly respawns you on death.", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(enabledUsers.contains(cs.getName()))
			enabledUsers.remove(cs.getName());
		else
			enabledUsers.add(cs.getName());
		cs.sendMessage(this.prefix + "You " + (enabledUsers.contains(cs.getName()) ? "en" : "dis") + "abled AutoRespawn module.");
		return true;
	}

}
