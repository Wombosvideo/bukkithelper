package core.bukkithelper.commands;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BfriendsHandler extends CommandHandler {

	private static final HashMap<String,ArrayList<String>> enabledUsers = new HashMap<String,ArrayList<String>>();

	public static void add(final Player p, final String s){
		enabledUsers.get(p.getName()).add(s.toLowerCase());
	}

	public static boolean contains(final Player p, final String s){
		return enabledUsers.get(p.getName()).contains(s.toLowerCase());
	}

	public static void eject(final Player p){
		enabledUsers.remove(p.getName());
	}

	public static void insert(final Player p){
		enabledUsers.put(p.getName(), new ArrayList<String>());
	}

	public static void remove(final Player p, final String s){
		enabledUsers.get(p.getName()).remove(s.toLowerCase());
	}

	public BfriendsHandler() {
		super("bfriends", "/bfriends help", "Internal friends list.", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length < 1){
			cs.sendMessage("§cWrong usage. See /bfriends help for further information.");
			return true;
		}
		final Player p = (Player)cs;
		if(args[0].equalsIgnoreCase("help")){
			p.sendMessage("§7------[ §a/bfriends usage list§7 ]------");
			p.sendMessage("§2/bfriends add <player> §7-§a Adds a friend.");
			p.sendMessage("§2/bfriends remove <player> §7-§a Removes a friend.");
			p.sendMessage("§2/bfriends list §7-§a Lists all friends.");
			p.sendMessage("§7------------------------------------");
		}else if(args[0].equalsIgnoreCase("add")){
			if(!(args.length > 1))
				cs.sendMessage("§cWrong usage. See /bfriends help for further information.");
			else if(contains(p,args[1]))
				p.sendMessage("§cYou are trying to add a friend which is already a friend.");
			else{
				add(p,args[1]);
				p.sendMessage("Successfully added \"" + args[1] + "\" to your friends list.");
			}
		}else if(args[0].equalsIgnoreCase("remove")){
			if(!(args.length > 1))
				cs.sendMessage("§cWrong usage. See /bfriends help for further information.");
			else if(!contains(p,args[1]))
				p.sendMessage("§cYou are trying to remove a friend which is not a friend.");
			else{
				remove(p,args[1]);
				p.sendMessage("Successfully removed \"" + args[1] + "\" from your friends list.");
			}
		}else if(args[0].equalsIgnoreCase("list")){
			String list = "";
			for(final String user : enabledUsers.get(p.getName()))
				list = list == "" ? user.toLowerCase() : list + ", " + user.toLowerCase();
				p.sendMessage(list == "" ? "§cYou don't have any friends, sorry :/" : "Here is a list of your friends: " + list);
		} else
			cs.sendMessage("§cWrong usage. See /bfriends help for further information.");
		return true;

	}

}
