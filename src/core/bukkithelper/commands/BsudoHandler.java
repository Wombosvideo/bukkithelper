package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import core.bukkithelper.versionbased.VersionBase;

public class BsudoHandler extends CommandHandler {

	public BsudoHandler() {
		super("bsudo", "/bsudo <player> <message...>", "Sends a message as a player.");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length < 2)
			cs.sendMessage("§cWrong usage: " + this.getUsage());
		else{
			final Player r = VersionBase.getPlayer(args[0]);
			if(r.equals(null)){
				cs.sendMessage(this.prefix + "§cYou want to send a message as a player which is not online!");
				return true;
			}
			String evilShit = "";
			for(final String s : args)
				evilShit = evilShit.equalsIgnoreCase("") ? s : evilShit + " " + s;
			evilShit = evilShit.replaceFirst(evilShit.startsWith(args[0] + " ") ? args[0] + " " : args[0], "");
			r.chat(evilShit);
		}
		return true;
	}



}
