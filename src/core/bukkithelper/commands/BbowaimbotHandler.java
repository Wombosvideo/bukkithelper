package core.bukkithelper.commands;

import java.util.HashMap;

import org.bukkit.command.CommandSender;

public class BbowaimbotHandler extends CommandHandler {

	public static final HashMap<String,String> enabledUsers = new HashMap<String,String>();

	public BbowaimbotHandler() {
		super("bbowaimbot", "/bbowaimbot [p|e|a]", "Automaticly redirects arrows to nearest entities.", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length == 0){
			if(enabledUsers.containsKey(cs.getName()))
				enabledUsers.remove(cs.getName());
			else
				enabledUsers.put(cs.getName(), "a");
		} else if(args.length == 1 && (args[0].equalsIgnoreCase("a") || args[0].equalsIgnoreCase("e") || args[0].equalsIgnoreCase("p")))
			if(enabledUsers.containsKey(cs.getName()))
				enabledUsers.remove(cs.getName());
			else
				enabledUsers.put(cs.getName(), args[0].toLowerCase());
		else{
			cs.sendMessage(this.prefix + "§cThe argument '" + args[0] + "' is not availlable. Use §4a for all enimies§c, §4e for entities§c, §4p for players§c.");
			return true;
		}

		String mode = enabledUsers.containsKey(cs.getName()) ? enabledUsers.get(cs.getName()) : "";
		switch(mode){
		case "a": mode = "all enimies"; break;
		case "e": mode = "entities"; break;
		case "p": mode = "players"; break;
		default: mode = "";
		}
		cs.sendMessage(this.prefix + "You " + (enabledUsers.containsKey(cs.getName()) ? "en" : "dis") + "abled BowAimbot module." + (mode != "" ? " (Attacks " + mode + ")" : ""));
		return true;
	}

}
