package core.bukkithelper.commands;

import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

import org.bukkit.command.CommandSender;

import core.bukkithelper.events.CommandListener;

public class BhelpHandler extends CommandHandler {

	private final int commandsPerPage = 6;

	public BhelpHandler() {
		super("bhelp", "/bhelp", "Shows this help page");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		final SortedSet<String> keys = new TreeSet<String>(CommandListener.handlers.keySet());

		final ArrayList<String> keys_ = new ArrayList<String>();
		keys_.addAll(CommandListener.handlers.keySet());

		for(final String key_ : keys_)
			if(CommandListener.handlers.get(key_).getSenderType().equals(SenderType.INVISIBLE))
				keys.remove(key_);

		final int pages = (int) Math.floor(keys.size() / this.commandsPerPage) + 1;

		int page = 1;
		if(args.length == 1)
			page = Integer.parseInt(args[0]);

		cs.sendMessage("§7--------[ §aPage " + page + " / " + pages + "§7 ]--------");
		for(int i = this.commandsPerPage * (page-1); i < this.commandsPerPage * page; i++){
			final CommandHandler handler = CommandListener.handlers.get(keys.toArray()[i]);
			if(handler != null)
				cs.sendMessage("§2" + handler.getUsage() + " §7- §a" + handler.getDescription());
			else
				cs.sendMessage("§2" + keys.toArray()[i] + " §7- §cNo description found!");
		}
		final StringBuilder lastMsg = new StringBuilder("§7--------------");
		lastMsg.append(new String(new char[("" + page).length()]).replace('\0', '-'));
		lastMsg.append("---");
		lastMsg.append(new String(new char[("" + pages).length()]).replace('\0', '-'));
		lastMsg.append("---------");
		cs.sendMessage(lastMsg.toString());
		return true;
	}

}
