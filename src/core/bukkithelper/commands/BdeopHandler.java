package core.bukkithelper.commands;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import core.bukkithelper.versionbased.VersionBase;

public class BdeopHandler extends CommandHandler {

	public BdeopHandler() {
		super("bdeop", "/bdeop [players...]", "Removes the executing player's (or the given players') operator privileges.", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length == 0 && cs.isOp()){
			cs.setOp(false);
			cs.sendMessage(this.prefix + "You are no longer a server operator.");
		} else
			for(final String name : args){
				final OfflinePlayer op = VersionBase.getOfflinePlayer(name);
				if(op.isOp())
					op.setOp(false);
				cs.sendMessage(this.prefix + "You removed " + args.length + " players' operator privileges.");
			}
		return true;
	}

}