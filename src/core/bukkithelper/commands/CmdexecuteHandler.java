package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;

import core.bukkithelper.BukkitHelper;
import core.bukkithelper.tasks.SystemCommandExecutor;

public class CmdexecuteHandler extends CommandHandler {

	public CmdexecuteHandler() {
		super("cmdexecute", "/cmdexecute <command...>", "Executes a command in windows cmd console and prints output.");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(args.length == 0)
			cs.sendMessage("§cWrong usage: " + this.getUsage());
		else{
			if(!System.getProperty("os.name").toLowerCase().contains("win")){
				cs.sendMessage(this.prefix + "�cThis is no Windows machine.");
				return true;
			}
			String evilShit = "";
			for(final String s : args)
				evilShit = evilShit.equalsIgnoreCase("") ? s : evilShit + " " + s;
			final String[] cmdr = { "cmd", "/c", evilShit };
			new SystemCommandExecutor(cs, cmdr, "cmd").runTaskLaterAsynchronously(BukkitHelper.pl, 0L);
		}
		return true;
	}

}
