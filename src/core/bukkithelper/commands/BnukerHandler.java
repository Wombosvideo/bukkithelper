package core.bukkithelper.commands;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import core.bukkithelper.BukkitHelper;

public class BnukerHandler extends CommandHandler {

	public static final ArrayList<String> enabledUsers = new ArrayList<String>();

	public static void nuke(final Player p) {
		if(p.getGameMode().equals(GameMode.SURVIVAL) && !p.isSneaking())
			createSolidClouds(p.getLocation());
		createFallingSphere(p.getLocation(), 4);
	}

	private static void createFallingSphere(final Location loc, final int radius){
		final int bX = loc.getBlockX();
		final int bY = loc.getBlockY();
		final int bZ = loc.getBlockZ();
		final boolean hollow = true;

		for (int x = bX - radius; x <= bX + radius; x++)
			for (int y = bY - radius; y <= bY + radius; y++)
				for (int z = bZ - radius; z <= bZ + radius; z++) {
					final double distance = (bX - x) * (bX - x) + (bZ - z) * (bZ - z) + (bY - y) * (bY - y);

					if (distance < radius * radius && !(hollow && distance < (radius - 1) * (radius - 1))) {
						final Location l = new Location(loc.getWorld(), x, y,z);
						if(l.getBlock().getType() != Material.AIR)
							if (!l.getBlock().hasMetadata("bukkithelper_snowball")) {
								final Material mat = l.getBlock().getType();
								final byte data = l.getBlock().getData();
								final FallingBlock b = l.getWorld().spawnFallingBlock(l, mat, data);
								b.setVelocity(randomVector());
								b.setDropItem(false);
								l.getWorld().playSound(l, Sound.STEP_STONE, 1, 1);
								l.getBlock().setType(Material.AIR);
							}
					}
				}
	}

	private static void createSolidClouds(final Location loc){
		final Block center = loc.getBlock().getRelative(BlockFace.DOWN);
		center.setType(Material.GLASS);
		center.setMetadata("bukkithelper_cloud", new FixedMetadataValue(BukkitHelper.pl, true));

		final Block n = center.getRelative(BlockFace.NORTH);
		n.setType(Material.GLASS);
		n.setMetadata("bukkithelper_cloud", new FixedMetadataValue(BukkitHelper.pl, true));

		final Block s = center.getRelative(BlockFace.SOUTH);
		s.setType(Material.GLASS);
		s.setMetadata("bukkithelper_cloud", new FixedMetadataValue(BukkitHelper.pl, true));

		final Block e = center.getRelative(BlockFace.EAST);
		e.setType(Material.GLASS);
		e.setMetadata("bukkithelper_cloud", new FixedMetadataValue(BukkitHelper.pl, true));

		final Block w = center.getRelative(BlockFace.WEST);
		w.setType(Material.GLASS);
		w.setMetadata("bukkithelper_cloud", new FixedMetadataValue(BukkitHelper.pl, true));

		final Block ne = center.getRelative(BlockFace.NORTH_EAST);
		ne.setType(Material.GLASS);
		ne.setMetadata("bukkithelper_cloud", new FixedMetadataValue(BukkitHelper.pl, true));

		final Block nw = center.getRelative(BlockFace.NORTH_WEST);
		nw.setType(Material.GLASS);
		nw.setMetadata("bukkithelper_cloud", new FixedMetadataValue(BukkitHelper.pl, true));

		final Block se = center.getRelative(BlockFace.SOUTH_EAST);
		se.setType(Material.GLASS);
		se.setMetadata("bukkithelper_cloud", new FixedMetadataValue(BukkitHelper.pl, true));

		final Block sw = center.getRelative(BlockFace.SOUTH_WEST);
		sw.setType(Material.GLASS);
		sw.setMetadata("bukkithelper_cloud", new FixedMetadataValue(BukkitHelper.pl, true));
	}

	private static Vector randomVector(){
		final int x = new Random().nextInt(3 - -3) + -3;
		final int z = new Random().nextInt(3 - -3) + -3;

		return  new Vector(x,0,z);
	}

	public BnukerHandler() {
		super("bnuker", "/bnuker", "Toggles your nuker module.", SenderType.PLAYER);
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(enabledUsers.contains(cs.getName()))
			enabledUsers.remove(cs.getName());
		else
			enabledUsers.add(cs.getName());
		cs.sendMessage(this.prefix + "You " + (enabledUsers.contains(cs.getName()) ? "en" : "dis") + "abled Nuker module.");
		return true;

	}

}
