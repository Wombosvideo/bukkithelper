package core.bukkithelper.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BseedHandler extends CommandHandler {

	public BseedHandler() {
		super("bseed", "/bseed", "Shows the seed of the current world.");
	}

	@Override
	public boolean onCommand(final String cmd, final CommandSender cs, final String[] args) {
		if(!(cs instanceof Player)){
			cs.sendMessage(this.prefix + "You have to be a player to execute this command.");
			return true;
		}
		final Player p = (Player)cs;
		cs.sendMessage("Seed for world " + p.getWorld().getName() + " is " + p.getWorld().getSeed());
		return true;
	}

}
