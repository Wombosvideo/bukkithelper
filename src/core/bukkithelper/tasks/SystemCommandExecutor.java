package core.bukkithelper.tasks;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;

import org.BadShit;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import core.bukkithelper.utils.Reflections;

public class SystemCommandExecutor extends BukkitRunnable {

	private static final String prefix = "�7[�aBukkitHelper�7] �a";
	private final CommandSender p;
	private final String[] cmdr;
	private final String cons;

	public SystemCommandExecutor(final CommandSender p, final String[] cmdr, final String console) {
		this.p = p;
		this.cmdr = cmdr;
		this.cons = console;
	}

	public void copy(final InputStream is, final StringWriter sw){
		final String name = "org.apache.commons.io.IOUtils";
		boolean a = false;
		try{
			Class.forName("net.minecraft.util." + name);
			a = true;
		}catch(final ClassNotFoundException e){}
		try {
			Reflections.getMethod(Class.forName((a ? "net.minecraft.util." : "") + name), "copy", is.getClass(), sw.getClass()).invoke(null, is, sw);
		} catch (final IllegalAccessException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		} catch (final IllegalArgumentException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		} catch (final InvocationTargetException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		} catch (final ClassNotFoundException e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		}
	}

	public String resolveColors(String s) {
		s = s.replaceAll("\\033\\[0;30m", "§0");
		s = s.replaceAll("\\033\\[0;34m", "§1");
		s = s.replaceAll("\\033\\[0;32m", "§2");
		s = s.replaceAll("\\033\\[0;36m", "§3");
		s = s.replaceAll("\\033\\[0;31m", "§4");
		s = s.replaceAll("\\033\\[0;35m", "§5");
		s = s.replaceAll("\\033\\[0;33m", "§6");
		s = s.replaceAll("\\033\\[0;37m", "§7");
		s = s.replaceAll("\\033\\[1;30m", "§8");
		s = s.replaceAll("\\033\\[1;34m", "§9");
		s = s.replaceAll("\\033\\[1;32m", "§a");
		s = s.replaceAll("\\033\\[1;36m", "§b");
		s = s.replaceAll("\\033\\[1;31m", "§c");
		s = s.replaceAll("\\033\\[1;35m", "§d");
		s = s.replaceAll("\\033\\[1;33m", "§e");
		s = s.replaceAll("\\033\\[1;37m", "§f");
		s = s.replaceAll("\\033\\[1;37m", "§f");
		s = s.replaceAll("\\033\\[0m", "§r");
		return s;
	}

	@Override
	public void run() {
		Process pr;
		try {
			this.p.sendMessage(prefix + "Executing command '" + this.cmdr[2] + "' in " + this.cons + "...");
			pr = Runtime.getRuntime().exec(this.cmdr);
			pr.waitFor();
			final InputStream es = pr.getErrorStream();
			final InputStream is = pr.getInputStream();
			final StringWriter esW = new StringWriter();
			final StringWriter isW = new StringWriter();
			this.copy(es, esW);
			this.copy(is, isW);
			final String esS = this.resolveColors(esW.toString());
			final String isS = this.resolveColors(isW.toString());
			boolean outany = false;
			boolean errors = false;
			boolean output = false;
			if(esS.equalsIgnoreCase("")) {
				outany = true;
				errors = true;
			} else if(isS.equalsIgnoreCase("")) {
				outany = true;
				output = true;
			}
			if(!outany)
				this.p.sendMessage(prefix + "The executed command returned no output!");
			else {
				if(output) {
					this.p.sendMessage(prefix + "The executed command returned this output:");
					this.p.sendMessage(" " + isS);
				}
				if(errors) {
					this.p.sendMessage(prefix + "The executed command returned this error:");
					this.p.sendMessage(" " + esS);
				}
			}
		} catch (final IOException e) {} catch (final InterruptedException e) {}

	}

}