package core.bukkithelper.tasks;

import org.bukkit.Effect;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import core.bukkithelper.BukkitHelper;

public class AimbotExecutor extends BukkitRunnable {

	Arrow arrow;
	LivingEntity target;

	public AimbotExecutor(final Arrow arrow, final LivingEntity target){
		this.arrow = arrow;
		this.target = target;
		this.runTaskTimer(BukkitHelper.pl, 0L, 1L);
	}

	@Override
	public void run() {
		final double speed = this.arrow.getVelocity().length();
		if (this.arrow.isOnGround() || this.arrow.isDead() || this.target.isDead()) {
			this.cancel();
			return;
		}
		final Vector toTarget = this.target.getLocation().clone().add(new Vector(0.0D, 0.5D, 0.0D)).subtract(this.arrow.getLocation()).toVector();
		final Vector dirVelocity = this.arrow.getVelocity().clone().normalize();
		final Vector dirToTarget = toTarget.clone().normalize();
		final double angle = dirVelocity.angle(dirToTarget);
		double newSpeed = 0.9D * speed + 0.14D;
		if (this.target instanceof Player && this.arrow.getLocation().distance(this.target.getLocation()) < 8.0D) {
			final Player player = (Player)this.target;
			if (player.isBlocking())
				newSpeed = speed * 0.6D;
		}
		Vector newVelocity;
		if (angle < 0.12D)
			newVelocity = dirVelocity.clone().multiply(newSpeed);
		else {
			final Vector newDir = dirVelocity.clone().multiply((angle - 0.12D) / angle).add(dirToTarget.clone().multiply(0.12D / angle));
			newDir.normalize();
			newVelocity = newDir.clone().multiply(newSpeed);
		}
		this.arrow.setVelocity(newVelocity.add(new Vector(0.0D, 0.03, 0.0D)));
		((Player)this.arrow.getShooter()).playEffect(this.arrow.getLocation(), Effect.SMOKE, 0);
	}



}
