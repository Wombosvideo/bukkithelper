package core.bukkithelper.tasks;

import java.util.ArrayList;

import core.bukkithelper.signs.TNTSign;

public class TNTSignExecutor extends TaskExecutor {

	public TNTSignExecutor() {
		super(0L, 5L);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		final ArrayList<TNTSign> list = (ArrayList<TNTSign>) TNTSign.SIGNS.clone();
		for(final TNTSign sign : TNTSign.SIGNS)
			if(sign.isExisting() && sign.isEnabled())
				sign.spawn();
			else if(!sign.isExisting())
				list.remove(sign);
		TNTSign.SIGNS.retainAll(list);
	}
}
