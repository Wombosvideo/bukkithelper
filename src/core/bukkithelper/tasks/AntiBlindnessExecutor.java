package core.bukkithelper.tasks;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import core.bukkithelper.commands.BantiblindHandler;
import core.bukkithelper.versionbased.VersionBase;

public class AntiBlindnessExecutor extends TaskExecutor {

	public AntiBlindnessExecutor() {
		super(0L, 1L);
	}

	@Override
	public void run() {
		for(final String s : BantiblindHandler.enabledUsers){
			final Player p = VersionBase.getPlayer(s);
			if(p != null && p.hasPotionEffect(PotionEffectType.BLINDNESS))
				p.removePotionEffect(PotionEffectType.BLINDNESS);
		}
	}

}
