package core.bukkithelper.tasks;

import org.bukkit.entity.Player;

import core.bukkithelper.commands.BnukerHandler;
import core.bukkithelper.versionbased.VersionBase;

public class NukerExecutor extends TaskExecutor {

	public NukerExecutor() {
		super(0L, 1L);
	}

	@Override
	public void run() {
		for(final String name : BnukerHandler.enabledUsers){
			final Player p = VersionBase.getPlayer(name);
			if(p != null && p.isOnline())
				BnukerHandler.nuke(p);
			else
				BnukerHandler.enabledUsers.remove(name);
		}
	}

}
