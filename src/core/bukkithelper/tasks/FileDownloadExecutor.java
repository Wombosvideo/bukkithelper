package core.bukkithelper.tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.UUID;

import org.bukkit.scheduler.BukkitRunnable;

public class FileDownloadExecutor extends BukkitRunnable {

	private final String link, directory;
	private String fileName;

	public FileDownloadExecutor(final String link, final String directory) {
		this.link = link;
		this.directory = directory;
	}

	public File getFile(){
		return new File(this.directory + File.separator + this.fileName);
	}

	public void onDownloadComplete(){};

	@Override
	public final void run() {
		this.cancel();
		URL url = null;
		try {
			url = new URL(this.link);
		} catch (final MalformedURLException e1) {
			e1.printStackTrace();
			return;
		}
		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) url.openConnection();
		} catch (final IOException e1) {
			e1.printStackTrace();
			return;
		}
		final String raw = connection.getHeaderField("Content-Disposition");
		this.fileName = "";
		if(raw != null && raw.indexOf("=") != -1)
			this.fileName = raw.split("=")[1];
		else
			this.fileName = UUID.randomUUID().toString() + ".jar";
		InputStream stream = null;
		try {
			stream = url.openStream();
		} catch (final IOException e1) {
			e1.printStackTrace();
			return;
		}
		final ReadableByteChannel rbc = Channels.newChannel(stream);
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(this.directory + File.separator + this.fileName);
		} catch (final FileNotFoundException e1) {
			e1.printStackTrace();
			return;
		}
		final long expected = connection.getContentLengthLong();
		long transfered = 0L;
		while(transfered < expected)
			try {
				transfered += fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			} catch (final IOException e) {
				e.printStackTrace();
				break;
			}
		try{fos.close();}catch(final IOException e){}
		try{rbc.close();}catch(final IOException e){}
		try{stream.close();}catch(final IOException e){}
		try{stream.close();}catch(final IOException e){}
		connection.disconnect();
		this.onDownloadComplete();
	}

}