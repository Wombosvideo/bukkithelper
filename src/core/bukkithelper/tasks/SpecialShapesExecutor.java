package core.bukkithelper.tasks;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import core.bukkithelper.BukkitHelper;

public class SpecialShapesExecutor extends BukkitRunnable {

	private final Player sender;

	public SpecialShapesExecutor(final Player player) {
		this.sender = player;
	}

	@Override
	public void run() {
		final Location loc = this.sender.getLocation();
		this.a(loc.clone().add(-10, 0, 0), 6);
		this.a(loc.clone().add(0, 22, 0), 7, 26);
		this.a(loc.clone().add(10, 0, 0), 6);
		loc.add(0, 48, 0);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitHelper.pl, new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < 2; i++) {
					final Entity entity = loc.getWorld().spawnEntity(loc, EntityType.SNOWBALL);
					final Vector v = new Vector(Math.random() * 0.5 * (Math.random() * 10 >= 5 ? -1 : 1), 1.25, Math.random() * 0.5 * (Math.random() * 10 >= 5 ? -1 : 1));
					entity.setVelocity(v);
					entity.setMetadata("bukkithelper_snowball", new FixedMetadataValue(BukkitHelper.pl, true));
				}
			}
		}, 0L, 1L);
	}

	/**
	 * from CockLibrary v0.1
	 * 
	 * This library was created by @Ingrim4, it allows you to build a cock
	 * in Minecraft :D
	 * 
	 * You are welcome to use it, modify it and redistribute it under the following
	 * conditions: 1. Don't claim this class as your own 2. Don't remove this text
	 * 
	 * @author Ingrim4
	 */
	private void a(final Location a, final int b) {
		for (int c = 1; c <= 360; c++)
			for (int d = 1; d <= 360; d++) {
				a.clone().add((int) Math.floor(Math.sin(d) * Math.cos(c) * b), (int) Math.floor(Math.sin(d) * Math.sin(c) * b), (int) Math.floor(Math.cos(d) * b)).getBlock().setType(Material.WOOL);
				a.getBlock().setData((byte)6);
			}
	}

	/**
	 * from CockLibrary v0.1
	 * 
	 * This library was created by @Ingrim4, it allows you to build a cock
	 * in Minecraft :D
	 * 
	 * You are welcome to use it, modify it and redistribute it under the following
	 * conditions: 1. Don't claim this class as your own 2. Don't remove this text
	 * 
	 * @author Ingrim4
	 */
	private void a(final Location a, final int b, final int c) {
		for (int d = 1; d <= 360; d++)
			for (int e = 1; e <= 360; e++) {
				final int f = (int) Math.floor(Math.sin(e) * Math.sin(d) * c);
				int g = (byte)(f > c * 0.5 ? 2 : 6);
				if (f < c / 8 * 6 && g == 2)
					g = Math.random() * 10 >= 5 ? 2 : 6;
					a.clone().add((int) Math.floor(Math.sin(e) * Math.cos(d) * b), f, (int) Math.floor(Math.cos(e) * b)).getBlock().setType(Material.WOOL);
					a.getBlock().setData((byte) g);
			}
	}

}
