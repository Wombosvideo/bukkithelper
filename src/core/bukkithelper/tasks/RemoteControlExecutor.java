package core.bukkithelper.tasks;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import core.bukkithelper.BukkitHelper;
import core.bukkithelper.versionbased.VersionBase;


public class RemoteControlExecutor extends BukkitRunnable {

	public final Player p, r;
	private final ItemStack[] con1, con2;
	private final GameMode mode;
	private final Location loc;
	private final int cursor, health, food;

	public RemoteControlExecutor(final Player controller, final Player remote) {
		this.p = controller;
		this.r = remote;
		this.con1 = this.p.getInventory().getContents();
		this.con2 = this.p.getInventory().getArmorContents();
		this.mode = this.p.getGameMode();
		this.loc = this.p.getLocation().clone();
		this.cursor = this.p.getInventory().getHeldItemSlot();
		this.health = VersionBase.getHealth(this.p);
		this.food = this.p.getFoodLevel();
		this.p.getInventory().setContents(this.r.getInventory().getContents());
		this.p.getInventory().setArmorContents(this.r.getInventory().getArmorContents());
		this.p.teleport(this.r.getLocation());
		this.p.setGameMode(remote.getGameMode());
		VersionBase.setHealth(this.p, VersionBase.getHealth(this.r));
		this.p.setFoodLevel(this.r.getFoodLevel());
		this.p.setFireTicks(0);
		this.p.getInventory().setHeldItemSlot(this.r.getInventory().getHeldItemSlot());
		this.p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE/2, 1, true), true);
		this.p.hidePlayer(this.r);
		this.r.hidePlayer(this.p);
		this.runTaskTimerAsynchronously(BukkitHelper.pl, 0L, 1L);
	}

	@Override
	public void run() {
		if(!this.p.isOnline()){
			this.cancel();
			return;
		}else if(!this.r.isOnline()){
			this.stop();
			return;
		}
		this.r.teleport(this.p.getLocation());
		this.r.getInventory().setContents(this.p.getInventory().getContents());
		this.r.getInventory().setArmorContents(this.p.getInventory().getArmorContents());
		this.r.getInventory().setHeldItemSlot(this.p.getInventory().getHeldItemSlot());
		this.r.setGameMode(this.p.getGameMode());
		VersionBase.setHealth(this.r, VersionBase.getHealth(this.p));
		this.r.setFoodLevel(this.p.getFoodLevel());
	}

	public void stop(){
		this.cancel();
		this.p.getInventory().setContents(this.con1);
		this.p.getInventory().setArmorContents(this.con2);
		this.p.setGameMode(this.mode);
		this.p.teleport(this.loc);
		this.p.removePotionEffect(PotionEffectType.INVISIBILITY);
		this.p.getInventory().setHeldItemSlot(this.cursor);
		VersionBase.setHealth(this.p, this.health);
		this.p.setFoodLevel(this.food);
		this.p.showPlayer(this.r);
		this.r.showPlayer(this.p);
	}

}
