package core.bukkithelper.tasks;

import java.util.ArrayList;

import org.bukkit.scheduler.BukkitRunnable;

public class ExecutorTask extends BukkitRunnable {

	private static final ArrayList<TaskExecutor> executors = new ArrayList<TaskExecutor>();

	public static final void add(final TaskExecutor exec){
		executors.add(exec);
	}

	@Override
	public void run() {
		for(final TaskExecutor exec : executors)
			exec.execute();
	}

}
