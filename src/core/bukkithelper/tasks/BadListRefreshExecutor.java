package core.bukkithelper.tasks;

import core.bukkithelper.utils.BadUserList;

public class BadListRefreshExecutor extends TaskExecutor {

	public BadListRefreshExecutor() {
		super(0L, 600L);
	}

	@Override
	public void run() {
		try{
			BadUserList.ins.synchronize();
		}catch(final Exception e){e.printStackTrace();}
	}
}
