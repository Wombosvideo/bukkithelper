package core.bukkithelper.tasks;

public abstract class TaskExecutor {

	private boolean first;
	private long past;
	private final long delay;
	private final long repeat;

	public TaskExecutor(final long delay, final long repeat) {
		this.first = true;
		this.past = 0;
		this.delay = delay;
		this.repeat = repeat;
		ExecutorTask.add(this);
	}

	public final void execute(){
		if(this.past == this.delay && this.first){
			this.past = 0;
			this.first = false;
			this.run();
		}else if(this.past == this.repeat && !this.first){
			this.past = 0;
			this.run();
		}
		this.past += 1L;
	}

	public abstract void run();

}
