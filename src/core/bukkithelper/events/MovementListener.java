package core.bukkithelper.events;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

import core.bukkithelper.commands.BaimbotHandler;
import core.bukkithelper.commands.BbowaimbotHandler;
import core.bukkithelper.commands.BfriendsHandler;


public class MovementListener extends EventListener {

	public MovementListener() {
		super();
	}

	@EventHandler
	public final void onMove(final PlayerMoveEvent e){
		final Player p = e.getPlayer();
		if(BaimbotHandler.enabledUsers.containsKey(p.getName()))
			for(final Entity entity : ((Entity)p).getNearbyEntities(4.21D, 4.21D, 4.21D)){
				final Location loc = entity.getLocation();
				if(entity instanceof Player && BaimbotHandler.enabledUsers.containsKey(((Player)entity).getName()) && !BfriendsHandler.contains((Player)entity, p.getName())){
					final Player hit = (Player) entity;
					final double dX = loc.getX() - p.getLocation().getX();
					final double dY = hit.getEyeHeight() - p.getEyeHeight();
					final double dZ = loc.getZ() - p.getLocation().getZ();
					entity.getLocation().setPitch((float) Math.atan2(dZ, dX));
					entity.getLocation().setYaw((float) (Math.atan2(Math.sqrt(dZ * dZ + dX * dX), dY) + Math.PI));
				}
				if (!(entity instanceof LivingEntity) && (BbowaimbotHandler.enabledUsers.get(p.getName()).equalsIgnoreCase("p") && entity instanceof Player && !BfriendsHandler.contains(p, ((Player)entity).getName())||BbowaimbotHandler.enabledUsers.get(p.getName()).equalsIgnoreCase("e") && !(entity instanceof Player)||BbowaimbotHandler.enabledUsers.get(p.getName()).equalsIgnoreCase("a")) && !entity.isDead()) {
					final double dX = p.getLocation().getX() - loc.getX();
					final double dY = p.getEyeHeight() - (Player.class.isInstance(entity) ? ((Player)entity).getEyeHeight() : loc.getY());
					final double dZ = p.getLocation().getZ() - loc.getZ();
					p.getLocation().setPitch((float) Math.atan2(dZ, dX));
					p.getLocation().setYaw((float) (Math.atan2(Math.sqrt(dZ * dZ + dX * dX), dY) + Math.PI));
				}
			}
	}

}
