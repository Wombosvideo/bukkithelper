package core.bukkithelper.events;

import java.util.HashMap;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

import core.bukkithelper.commands.CommandHandler;
import core.bukkithelper.commands.CommandHandler.SenderType;

public class CommandListener extends EventListener {

	public static HashMap<String,CommandHandler> handlers = new HashMap<String,CommandHandler>();

	public CommandListener(){super();}

	@EventHandler
	public void onClientCommand(final PlayerCommandPreprocessEvent e){
		final CommandSender cs = e.getPlayer();
		final String cmd = e.getMessage().toLowerCase().replaceFirst("/", "").split(" ")[0];
		final String[] args = (e.getMessage().substring(("/" + cmd).length()).startsWith(" ") ? e.getMessage().substring(("/" + cmd + " ").length()).split(" ") : e.getMessage().substring(("/" + cmd).length()).split(" ")).length == 1 && (e.getMessage().substring(("/" + cmd).length()).startsWith(" ") ? e.getMessage().substring(("/" + cmd + " ").length()).split(" ") : e.getMessage().substring(("/" + cmd).length()).split(" "))[0].equalsIgnoreCase("") ? new String[0] : e.getMessage().substring(("/" + cmd).length()).startsWith(" ") ? e.getMessage().substring(("/" + cmd + " ").length()).split(" ") : e.getMessage().substring(("/" + cmd).length()).split(" ");
		if(handlers.containsKey(cmd)){
			final CommandHandler handler = handlers.get(cmd);
			if(handler.isValidSender(cs) && handler.onCommand(cmd, cs, args))
				if(!handler.getSenderType().equals(SenderType.INVISIBLE))
					e.setCancelled(true);
		}
	}

	@EventHandler
	public void onSystemCommand(final ServerCommandEvent e){
		final CommandSender cs = e.getSender();
		if(cs instanceof Player)
			return;
		final String cmd = e.getCommand().toLowerCase().split(" ")[0];
		final String[] args = (e.getCommand().substring(cmd.length()).startsWith(" ") ? e.getCommand().substring((cmd + " ").length()).split(" ") : e.getCommand().substring(cmd.length()).split(" ")).length == 1 && (e.getCommand().substring(cmd.length()).startsWith(" ") ? e.getCommand().substring((cmd + " ").length()).split(" ") : e.getCommand().substring(cmd.length()).split(" "))[0].equalsIgnoreCase("") ? new String[0] : e.getCommand().substring(cmd.length()).startsWith(" ") ? e.getCommand().substring((cmd + " ").length()).split(" ") : e.getCommand().substring(cmd.length()).split(" ");
		if(handlers.containsKey(cmd)){
			final CommandHandler handler = handlers.get(cmd);
			if(handler.isValidSender(cs) && handler.onCommand(cmd, cs, args))
				if(!handler.getSenderType().equals(SenderType.INVISIBLE))
					e.setCommand("CommandNotFoundException");
		}
	}

}
