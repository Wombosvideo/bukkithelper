package core.bukkithelper.events;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.BadShit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import core.bukkithelper.commands.BautorespawnHandler;
import core.bukkithelper.commands.BremoteHandler;

public class DeathListener extends EventListener {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void autoRespawnPlayer(final Player who) throws ReflectiveOperationException {
		final String bukkitversion = Bukkit.getServer().getClass().getPackage().getName().substring(23);
		final Class<?> cp = Class.forName("org.bukkit.craftbukkit." + bukkitversion + ".entity.CraftPlayer");
		final Class<?> clientcmd = Class.forName("net.minecraft.server." + bukkitversion + ".PacketPlayInClientCommand");
		final Class enumClientCMD = Class.forName("net.minecraft.server." + bukkitversion + ".EnumClientCommand");
		final Method handle = cp.getDeclaredMethod("getHandle", new Class<?>[0]);
		final Object entityPlayer = handle.invoke(who, new Object[0]);
		final Constructor<?> packetconstr = clientcmd.getDeclaredConstructor(enumClientCMD);
		final Enum<?> num = Enum.valueOf(enumClientCMD, "PERFORM_RESPAWN");
		final Object packet = packetconstr.newInstance(num);
		final Object playerconnection = entityPlayer.getClass() .getDeclaredField("playerConnection").get(entityPlayer);
		final Method send = playerconnection.getClass().getDeclaredMethod("a", clientcmd);
		send.invoke(playerconnection, packet);
	}

	public DeathListener() {
		super();
	}


	@EventHandler
	public void onDeath(final PlayerDeathEvent e){
		final Player p = e.getEntity();
		if(BautorespawnHandler.enabledUsers.contains(p.getName())){
			Bukkit.getPluginManager().callEvent(new PlayerRespawnEvent(p, p.getBedSpawnLocation(), true));
			try {
				autoRespawnPlayer(p);
			} catch (final ReflectiveOperationException e1) {
			}
		}
	}

	@EventHandler
	public void onRespwn(final PlayerRespawnEvent e){
		if(BremoteHandler.enabledUsers.containsKey(e.getPlayer().getName()))
			try {
				autoRespawnPlayer(BremoteHandler.enabledUsers.get(e.getPlayer().getName()).r);
			} catch (final ReflectiveOperationException e1) {
				if(BadShit.DEBUG)
					e1.printStackTrace();
			}
	}
}
