package core.bukkithelper.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import core.bukkithelper.commands.BfriendsHandler;

public class JoinLeaveListener extends EventListener {

	public JoinLeaveListener() {
		super();
	}

	@EventHandler
	public void onJoin(final PlayerJoinEvent e){
		BfriendsHandler.insert(e.getPlayer());
	}

	@EventHandler
	public void onQuit(final PlayerQuitEvent e){
		BfriendsHandler.eject(e.getPlayer());
	}

}
