package core.bukkithelper.events;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.util.Vector;

import core.bukkithelper.commands.BbowaimbotHandler;
import core.bukkithelper.commands.BfriendsHandler;
import core.bukkithelper.tasks.AimbotExecutor;

public class ShootBowListener extends EventListener {

	public ShootBowListener() {
		super();
	}

	@EventHandler
	public void onEntityBow(final EntityShootBowEvent event){
		if(event.getEntity() instanceof Player && BbowaimbotHandler.enabledUsers.containsKey(((Player)event.getEntity()).getName()) && event.getProjectile() instanceof Arrow){
			final Player p = (Player) event.getEntity();
			double minAngle = 6.283185307179586D;
			double minDistance = 64D;
			Entity minEntity = null;
			for (final Entity entity : p.getNearbyEntities(64.0D, 64.0D, 64.0D))
				if (!(entity instanceof LivingEntity) && p.hasLineOfSight(entity) && (BbowaimbotHandler.enabledUsers.get(p.getName()).equalsIgnoreCase("p") && entity instanceof Player && !BfriendsHandler.contains(p, ((Player)entity).getName())||BbowaimbotHandler.enabledUsers.get(p.getName()).equalsIgnoreCase("e") && !(entity instanceof Player)||BbowaimbotHandler.enabledUsers.get(p.getName()).equalsIgnoreCase("a")) && !entity.isDead()) {
					final Vector toTarget = entity.getLocation().toVector().clone().subtract(p.getLocation().toVector());
					final double angle = event.getProjectile().getVelocity().angle(toTarget);
					final double distance = event.getProjectile().getLocation().distance(entity.getLocation());
					if (angle < minAngle && distance < minDistance) {
						minAngle = angle;
						minEntity = entity;
						minDistance = distance;
					}
				}
			if (minEntity != null)
				new AimbotExecutor((Arrow)event.getProjectile(), (LivingEntity)minEntity);
		}
	}

}