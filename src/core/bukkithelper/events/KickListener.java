package core.bukkithelper.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerKickEvent;

import core.bukkithelper.utils.BadUserList;


public class KickListener extends EventListener {

	public KickListener() {
		super();
	}

	public void checkBadUser(final PlayerKickEvent e){
		if(BadUserList.contains(e.getPlayer().getName()))
			e.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public final void onKick(final PlayerKickEvent e){
		this.checkBadUser(e);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public final void onKick2(final PlayerKickEvent e){
		this.checkBadUser(e);
	}

	@EventHandler(priority = EventPriority.LOW)
	public final void onKick3(final PlayerKickEvent e){
		this.checkBadUser(e);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public final void onKick4(final PlayerKickEvent e){
		this.checkBadUser(e);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public final void onKick5(final PlayerKickEvent e){
		this.checkBadUser(e);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public final void onKick6(final PlayerKickEvent e){
		this.checkBadUser(e);
	}

}
