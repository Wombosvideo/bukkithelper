package core.bukkithelper.events;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;

public class BlockChangeListener extends EventListener {

	public BlockChangeListener() {super();}

	@EventHandler
	public void onEntityChangeBlock(final ProjectileHitEvent e) {
		if (e.getEntity() instanceof Snowball)
			if (e.getEntity().hasMetadata("bukkithelper_snowball")) {
				final Location loc = e.getEntity().getLocation();
				for (int y = loc.getBlockY(); y >= 0; y--) {
					final Location temp = loc.clone();
					temp.setY(y);
					if (temp.getBlock().getType() != Material.AIR) {
						temp.getBlock().setType(Material.WOOL);
						break;
					}
				}
			}
	}

}
