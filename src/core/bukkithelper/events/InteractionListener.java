package core.bukkithelper.events;

import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import core.bukkithelper.signs.TNTSign;

public class InteractionListener extends EventListener {

	public InteractionListener() {
		super();
	}

	@EventHandler
	public void onInteract(final PlayerInteractEvent e){
		if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
			if(e.getClickedBlock() != null && e.getClickedBlock().getState() instanceof Sign){
				final TNTSign sign = TNTSign.get(e.getClickedBlock().getLocation());
				if(sign != null)
					sign.setEnabled(((Sign)e.getClickedBlock().getState()).getLine(1).equalsIgnoreCase("Disabled"));
			}
	}

}
