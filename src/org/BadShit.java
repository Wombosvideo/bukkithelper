package org;

import java.io.IOException;
import java.net.URISyntaxException;

public class BadShit {

	public static final boolean DEBUG = false;
	public static BadShit ins;
	public static void main(final String[] args) {
		BadShit.ins = new BadShit(args);
	}

	public final String plugins;

	public BadShit(final String[] args) {
		System.out.println("Initializing drivers...");
		this.plugins = this.getPluginFolder(args);
		try {
			new InternalInjector().inject();
		} catch (IOException | URISyntaxException e1) {
			e1.printStackTrace();
		}
		System.out.println("Launching server...");
		try {
			org.bukkit.craftbukkit.Main.main(args);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private String getPluginFolder(final String[] args) {
		for (int i = 0; i < args.length; i++)
			if (args[i].equalsIgnoreCase("--plugins") || args[i].equalsIgnoreCase("-P"))
				return args[i + 1];
		return "plugins";
	}

}
